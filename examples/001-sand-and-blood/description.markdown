---
title: Description
---

**Can the power of the weak save them all?**

Growing up a disappointment, Shimusogo Rutejìmo has always struggled with proving himself worthy to his family and clan. All he wants is the magic to run faster than the strongest warrior, emulating his brother's strength and courage. When he is once again caught showcasing his poor decisions and ineptitude, he's sent on a quest for his manhood, a discovery of his true bravery and worth.

His journey proves perilous and contrived as the elders who were to guide his endeavors abandon him in the dead of the night, forcing him to forge on without the tutelage he needs to succeed. When danger begins to envelop him, it's up to Rutejìmo to find a way to not only gain inner courage and confidence, but to bravely save the friends he's encountered along way. But he'll need the clan spirit's ultimate speed to conquer the impossible. Can a meek man find the strength to fight for himself?
