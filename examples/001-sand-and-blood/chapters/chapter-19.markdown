---
when:
  start: 1471/3/47 MTR 10::13
date: 2012-09-09
title: Humiliated
locations:
  primary:
    - Lonely Stretches of Jade
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Shimusògo
    - Tateshyúso
  referenced:
    - Mikáryo
    - Karawàbi
    - Tejíko
    - Desòchu
organizations:
  secondary:
    - Shimusògo
    - Tateshyúso
summary: >
  Rutejìmo woke up still humiliated by his actions from the night before when Mikáryo attacked. For the first time, he wanted to run, so he ran off until he felt better. He managed to move fast enough he came up to another stopping point where he spotted signs of the other Shimusògo.

  When he got back, he and Pidòhu spoke about the nature of Chimípu becoming a warrior. Rutejìmo didn't know about the consequences including sterility, inability to marry, and an almost guaranteed death in battle.

  Pidòhu also confided that he will never become a Shimusògo. Instead, he kept seeing a giant shadow sailing across the desert and thought that it was his path.

  Chimípu came back and Rutejìmo offered a knife to replace the one that was ruined during the fight with Mikáryo. Chimípu accepted it.
---

> The worst critic is the voice inside your head. --- Kormar Proverb

Rutejìmo woke up thinking about his actions the night before. The humiliation still burned bright, and he kept replaying the encounter in his head, pretending he wasn't as pathetic as Mikáryo said he was. But, no matter how dramatic his fantasies, there was no way to take back what he had done. He had failed them. Any hopes of being as good as Chimípu were blown away in a single night; he could never fight in the darkness that way, blind but somehow defending the clan against a superior opponent.

He sniffed and looked at the others. Pidòhu was sitting up, but his body shook with every movement. The injured teenager kept wiping his brow as he struggled to hold the water skin to his lips.

Chimípu knelt down next to him, her gaze fixed on him. She held her hand underneath the water skin, ready to catch it if it fell. She spoke to him quietly, a whisper too soft for Rutejìmo to hear.

Pidòhu said something and she smirked.

Rutejìmo felt ostracized by the only clan he had. He got up and trudged away, to answer the pressure in his bladder and to avoid the people who had seen him at his weakest.

His feet scuffed on the sand and rocks. He headed back to the rock they spent the first part of the night under. He couldn't stain it any worse, and it gave him privacy.

Thoughts spiraling into depression, he finished what he needed to and circled around the rock. He had done everything wrong on the trip: didn't stop Karawàbi from knocking Pidòhu off of the Tooth, going with the wrong group, and then making a coward of himself when they needed him most. Rutejìmo wondered if he deserved to be a man, or to see the dépa.

For the first time, he wanted to run simply for the need to run. It was a strange feeling, but he hoped that it would clear the shadows from his thoughts. He glanced back at their new camp; Pidòhu and Chimípu were still talking. He shook his head and picked a direction and ran. He would only go a few chains at most and then come back.

Shimusògo appeared as he got up to speed, the little dépa sprinting ahead of him. Rutejìmo smiled into the wind. It felt right when he was chasing the clan spirit. He bore down and accelerated, racing after the bird he would never catch.

When he ran, he couldn't think. His mind grew empty until there was nothing but the dépa and the blur of the world. He kept on running, keeping along the curves of the rocks as they turned into dunes. When he hit the soft sand, he expected to stumble, but the sand was as solid as a rock. He found purchase even running along the sandy ridges.

Elation filled him and it spread out to suffuse his entire body. When he was running, he didn't feel like a fool and a coward. He felt like a runner, a courier of Shimusògo.

The dépa fluttered and it sprinted away from him. The feathered crest bounced with its movement as it left a trail of dust.

Rutejìmo stumbled, remembering how it had disappeared before the valley, but the bird was still visible. He regained his pace and pushed himself harder, struggling to catch up again. His legs and arms moved in an easy rhythm, but they burned with his efforts.

The world spun past him, a blur of rocks and sand. He chased the dépa, trusting the spirit to guide him to where he needed to go.

He didn't know how long he ran. Time just slipped by as he tried to keep up with the dépa. He focused on the bird, but the spirit was slowing him down. He thought he could catch up, but his own speed faltered with the bird's. Ahead of him, he saw a set of Wind's Teeth sticking out of the ground. There were five of them, like jagged fingers poking out of the desert.

Rutejìmo slid to his side and skidded to a halt. His hands and feet tore through the dunes, leaving a deep slash from his slowing. His momentum threw him into one of the towering rocks. He hit it with a thud and fell to the ground, stunned. He shook his head to clear the stars from his vision.

Images of Pidòhu's injuries flashed through his mind. Gasping, he sat up and felt his legs and hands, but he had not even a cut. The Tooth, on the other hand, had a large fragment broken off from the impact. Rutejìmo slumped back and chuckled. He wasn't even winded.

With a smile, he crawled to his feet. He spied the shard of rock that broke off upon his impact and picked it up. It was heavy. He hefted it before he felt the heat rolling off it. With a hiss, he tossed it aside before it burned him. It bounced off and came to a rest in a patch of disturbed sand.

Rutejìmo froze and looked around. He saw the remains of a camp. To his side, in a large circle, were impressions of tents and bags. Paths crossed over the sand, circling around the buried remains of a fire. He spotted some threads clinging to a sharp edge of a rock. They were the same oranges, reds, and yellows he grew up with: Shimusògo's colors.

Mikáryo's words came to mind. The clan was watching him.

Rutejìmo looked up, but he didn't see anyone. He gulped and turned back the way he came. He could see his path across the sand, but the wind was already erasing parts of it.

He felt a prickle of fear and hope. With a start, he ran. When the dépa appeared, he chased it but kept an eye on his surroundings. He watched for landmarks and tried to memorize each one as he passed.

He came up to the outcropping he had turned around and saw three tents still fluttering in the wind. He stumbled, but then kept on running, mapping out the route. There were some things that shouldn't be explored alone.

It felt like only ten minutes before he came up to their camp again. He circled around the rock and skidded to a halt next to it. His stopping was more precise, if only from practice, and he managed to avoid hitting the stone or tripping. A smile stretched across his face, and he hopped in a circle as excitement pumped through his veins.

He trotted back to Pidòhu.

"Good run?"

Rutejìmo sat down heavily next to him. The joy faded slightly. "Yeah," he said, "it was. I'm sorry, I should have told you."

"Shimusògo run." He said it as if it explained everything. After the last few days, it did. He felt better after running.

"Look, about last night…," he started.

Pidòhu shook his head, then stopped abruptly as he paled. He braced himself against the ground and took a deep breath. "Last night was last night. No reason to bring it up."

"I… made a fool of myself."

"Yeah, you did," murmured Pidòhu as he drank from his cup.

Rutejìmo colored, but Pidòhu kept speaking.

"But you've always been a fool, so nothing really changed."

Rutejìmo stared in shock, wondering if the quiet words were insulting or playful.

Pidòhu answered with a wink and a weak smile. He wiped the sweat from his face and onto his leg. There were already wet streaks on the fabric.

Letting out his breath, Rutejìmo chuckled. "Yeah, I seem to keep making a fool of myself."

"Here, Jìmo, eat." Pidòhu handed him the last of the bird from the previous night. "Mípu is catching more for later… and," he said with a grin, "probably running out her own frustrations."

"How can she be frustrated? She defended us." He sighed. "And Mikáryo thinks I'm useless."

"There is a lot more to the path she's on. She is at the crux of giving up a lot more to Shimusògo than you ever will."

Rutejìmo clamped his mouth shut, a surge of jealousy rising up. "What do you mean? She's going to be even better at magic?"

"Probably, but there is a cost."

"What?"

"Ever notice your brother never married?"

Rutejìmo shook his head. "What does that have to do with it?"

"Think, Jìmo. How many warriors have children?"

Rutejìmo frowned. He had never noticed it, but he realized there was only one. "Grandfather was a warrior."

"But he isn't your mother's father. Great Shimusogo Tejíko lost her first husband before you were born. The man you call grandfather was later, when her children had grown up and he was too injured to continue."

Curling his feet up, Rutejìmo stared at Pidòhu. "I know, but why…." Realization dawned. "He has no children, does he?"

Pidòhu shook his head. "Neither will your brother. And neither will Chimípu. A warrior's path is a very lonely, barren path."

"Why?"

"To defend us. They give everything to Shimusògo, and he gives them powers to protect our clan. Mípu knows that. Last night made it clear to her; she is going to lose everything for Shimusògo. And, I suspect, she already figured out the question but she needs to run around to come to the obvious answer. Like someone else I know."

"S-Should I do anything?"

Pidòhu pointed to him. "I'd say be yourself, but you can't help that. I'm going to say that our shared incompetence"---he paused for a deep breath---"is going to point out that she is herself, you are yourself, and I am me."

Rutejìmo blushed. "I'm a little slow, aren't I?"

Pidòhu sipped his cup again. "Yes."

Shooting a mock glare at Pidòhu, Rutejìmo said, "I could run you to the ground… if you could run."

"Yes," Pidòhu said over his cup, "but I'm not Shimusògo."

Icy cold ran down Rutejìmo's spine and his skin began to tingle. "W-What?"

"Shimusògo is never going to accept me."

"Nonsense, we'll just get you healed up---"

"Jìmo."

Frustrated, Rutejìmo continued, "---and then we'll run---"

"Jìmo!"

Rutejìmo clamped his mouth shut.

Pidòhu finished his cup and set it down. "I"---he pointed to himself---"am not Shimusògo."

"You can't give up, Dòhu!"

"I'm not." Pidòhu was smiling. "But I… look, do you see that?" He pointed out on the sand.

Rutejìmo peered out across the dunes lit up by the morning light. He saw nothing. "No."

"I see it. A large shadow of a raptor flying above us. It has been circling all morning."

Staring, Rutejìmo saw nothing. "Pidòhu, I think you are…." His voice trailed off as a large shadow crossed the sands. Behind it, the sand kicked up in little eddies of wind, dying down a heartbeat later.

He turned back to Pidòhu just as the injured boy was finishing a sweep of his hand. "What was that?"

"Tateshyúso."

Rutejìmo had heard the name before. It was the clan spirit to Jyotekábi, the frail woman who lived in the valley with two others. "Tateshyúso? Why would you be seeing Tateshyúso?"

"I don't know, but I can feel those shadows. And I realized that I can do this." He swept his hand, and a breeze rose up around Rutejìmo.

Rutejìmo caught a hint of movement in the corner of his eyes, but when he turned there was nothing. Confused, he turned back. "Can that happen? Can you be called by another clan spirit?"

Pidòhu shrugged. "Seems like it."

"Did you tell Mípu?"

"Tell me what?" asked Chimípu as she sat down. She had a brace of birds on her waist, already stripped of feathers and gutted.

"About the shadows across the sand," replied Pidòhu.

She glared at him and then at Rutejìmo. "You are pushing yourself too much, Dòhu. You are hallucinating."

"No, I think it's real. It is real. It is Tat---"

She knelt down, slapped the birds against a rock, and then spread them out.

The impact quieted Pidòhu, and he closed his mouth.

She looked up at him. "Dòhu, you need rest." Her tone allowed for no disagreement.

In the silence, she pulled out a travel bag filled with salt and spices. It was used to preserve meat for a few days. After shoving the carcasses inside, she wrapped them tightly before opening a small vial attached to the bag. She poured it inside. The liquid hissed loudly, and she tied the bag shut. When they stopped for the evening, time and movement would have cooked and seasoned the meat. She attached it to Pidòhu's stretcher.

When she finished, she stood up. "Ready to go?"

It was obvious Chimípu didn't want to talk about Tateshyúso.

Rutejìmo choked down his food even as he stood up. He swallowed hard before saying, "Yes, Great Shimusogo Chimípu."

Chimípu shielded her eyes and looked out across the desert. "Any idea which way to go?"

Rutejìmo smiled and pointed the direction he ran. "We camped about a day's drag away. Beyond that, there are some Wind's Teeth about a day past that. I saw signs of a camp there too, so it's probably safe."

She shot him a look, surprised but otherwise unreadable.

Rutejìmo kept his smile inside. "I-I had to run."

For a moment, she said nothing. Then she lowered her hand to her belt, to the empty sheath. She sighed and stared out in the direction he pointed.

Rutejìmo got an idea. He pulled out his knife before handing it to her hilt first.

Chimípu looked at it before she regarded him.

"You"---he realized it was easier to admit it---"you are a better fighter than me."

She took it with a quick bow. "Thank you, Great Shimusogo Rutejìmo."
