---
when:
  start: 1471/3/45 MTR 2::48
date: 2012-08-27
title: A Quiet Conversation
locations:
  primary:
    - Three Falls Teeth
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
  referenced:
    - Shigáto
    - Tsubàyo
    - Karawàbi
    - Shimusògo
organizations:
  secondary:
    - Shimusògo
summary: >
  Rutejìmo and Pidòhu talked in the middle of the night about the nature of magic, the struggles of growing up, and how the rites of passage work. Pidòhu reveals that Chimípu had already manifested her powers before the rites. It happened after her mother died and before they made the vote to start the rite of passage. The rite was to accept her into the clan, even if the spirit had already done so.
---

> Honest conversations happen when no one is listening. --- Badenfumi Shigáto

Rutejìmo sat in the darkness outside of the tent. He was exhausted, but it was his turn to stand guard while Pidòhu and Chimípu slept. He couldn't see more than a few inches past his nose in the dim light of the glow eggs, but he kept his ears open and listened for intruders. They didn't know if Tsubàyo or Karawàbi had followed him back, but Chimípu didn't want to take chances.

The cold wind of the desert nipped at his skin. The only thing keeping him from shivering was the heated rock between his legs. They used the last of the alchemical fire to heat six rocks for the night: four for Pidòhu, one for Chimípu, and one for himself. The latent heat would help a little against the cold, but as he sat in the darkness, he couldn't help but feel every frigid caress of the night winds.

He struggled to keep his mind from a spiral of depression. Chimípu had allowed him to remain, but she had made it clear that she didn't accept him yet. She used insulting terms---boy, bastard, idiot---instead of his name. His anger prickled at it, but he kept it buried deep inside. He had bared his neck to her, trusted her, and she spared him. He could survive being called an idiot for that.

Next to him, Pidòhu moaned in pain and his leg shook. The blanket covering him slipped off and piled up against Rutejìmo's leg.

Working carefully, Rutejìmo eased it back over the boy's injured leg. His fingers accidentally brushed against Chimípu's thigh, and he snatched his hand back before she took off his hand.

"Jìmo?" asked Pidòhu in a croaked whisper.

Rutejìmo jumped in surprise, then leaned against the rock. "Yes, Dòhu?"

"How did you come back?"

When Rutejìmo considered telling Tsubàyo about the dépa, something had stopped him. But, when Pidòhu asked, there was no reluctance. Taking a deep breath, he crossed his arms over his chest before answering. "I followed a shimusogo dépa."

"A dépa?"

Rutejìmo nodded, then flushed when he realized Pidòhu couldn't see it. "Yeah. I was struggling to keep up with Tsubàyo as we… I left here. They wouldn't slow down or stop. But then I saw this dépa running near me. When I followed it, I could concentrate on keeping up with it. When I stopped I… lost sight of it."

"And today?"

"It led me here. It ran and I followed."

Pidòhu let out a pained gasp. "That's Shimusògo, you know."

Rutejìmo nodded. "I guessed. But why didn't any of the elders tell us what to expect? If I knew that it should show up when I ran faster, I'd… run faster, I guess."

"Being told how to see a spirit doesn't make it any easier to actually see them. Actually, they say your expectations make it harder for you to listen. So, if they told you, you would already have an expectation of what to see. And then you wouldn't be able to see it if it wasn't exactly the same way."

"You knew this was going to happen, right?"

A grunt.

"You knew what to expect."

Pidòhu groaned and started to sit up. After a second, he slumped back. "I had an idea, but I haven't seen Shimusògo."

"Has Mípu?"

"What do you think?" There was a wry tone in Pidòhu's voice.

"How long has she been seeing Shimusògo?" Rutejìmo glanced over toward Chimípu, but he couldn't tell if she was listening or not. He couldn't even see her.

"Oh, I'm going to say the run she went on when her mother died. She was crying when she left and when she got back… well, ever since she's had a new confidence. And she kept up with the elders far better than any of us."

Rutejìmo stared out, his thoughts spinning. "Then this was all for nothing? I thought we were doing her rite of passage."

"Actually, this trip is for all of us. Some of us, Tsubàyo and Karawàbi, I hope never make it. But, even if Shimusògo speaks to Chimípu, the elders still need to do this before they can accept it."

"Can't Shimusògo just tell them that he talks to Mípu?"

"Sometimes, you have so much sand in your head that you can't hear your spirits. It is just what it is. There are two parts to the rites: becoming a member of the clan and becoming a man, or a woman in Mípu's case. You and she both have talked to Shimusògo, you can hear him or see him, if you want. What you do with that power determines if you become one of the clan."

"Damn." Rutejìmo leaned back and sighed. "Why can't it be easy?"

"If it was easy, it wouldn't be worth it."

Rutejìmo chuckled. "True as sand."

Neither said anything for a long moment. The rock behind Rutejìmo was warmer than his skin, and he basked in the fading heat as he listened to the desert around him.

"Pidòhu?"

"Yes, Jìmo?"

"Do you think you'll see Shimusògo?"

A long sigh. "I don't know. Shimusògo runs and I cannot run right now. I keep looking for him, but I won't get speed on this trip. Instead, the only things I see are shadows racing across the sands."

"What does that mean?"

"I don't know," Pidòhu said with a sigh. "I don't know if I'm just seeing things or if they are actually there. Mípu can't see it. And, if I'm hallucinating, then I wished I knew why."

There was a long, uncomfortable silence. Rutejìmo toyed with his hand for a moment. Then he accidentally blurted out his thoughts. "Karawàbi peed on your poetry book."

"That," Pidòhu sounded sad, "was my father's."

"I'm sorry."

"I saw you didn't have my pack either." There was an unasked question in the air.

Rutejìmo sat up and reached out for his bag. He dug into the bottom and got out the pouch with the stones he had transferred. Working blindly, he pulled out Pidòhu's. They were cold in his palm. He caught Pidòhu's hand and placed the rocks inside.

Pidòhu let out a sigh, and the rocks clinked together. "Thank you, Rutejìmo."

"I'm sorry I left."

"I already forgave you."

"I haven't forgiven myself, though."

"That," Pidòhu said with a chuckle, "can take a lot longer."
