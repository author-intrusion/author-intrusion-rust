---
when:
  start: 1471/3/43 MTR 11::26
date: 2012-06-22
title: Blood and Bone
locations:
  primary:
    - Three Falls Teeth
characters:
  primary:
    - Rutejìmo
  secondary:
    - Pidòhu
    - Karawàbi
    - Tsubàyo
    - Chimípu
  referenced:
    - Byomími
    - Tachìra
summary: >
  As Pidòhu cried out in pain, Tsubàyo refused to help him. Rutejìmo froze, unable to do anything. He kept looking at Tsubàyo to take charge, but the other teenager turned his back on Pidòhu and ran off.

  Chimípu came back to find the damage. She lashed out at Rutejìmo, calling him a coward. Later, she went after Tsubàyo and Karawàbi, but came back bruised and angry.

  She gave Rutejìmo a choice to go with Tsubàyo and Karawàbi, who were packing to head home, or stay. With her anger toward him, Rutejìmo joined the others and left Chimípu and Pidòhu.
---

> Actions made in a moment of unconscious reaction are worth more than a thousand lectures of note. --- Yunujyoraze Byomími

For a long moment, no one said anything. A few rocks bounced off the Wind's Tooth and hit the ground with heavy smacks. One of them, slightly larger than Rutejìmo's fist, struck Pidòhu in the back. The meaty thud only brought a shudder before it rolled off into the sand. It left a blood-flecked wake behind, before it stopped a few inches away. The wind had quieted, and nothing obscured the sound of Pidòhu's groans.

Rutejìmo stared at Pidòhu's limp body. He had never seen someone seriously hurt before. He kept replaying the sight of his body plummeting to the ground and the sound of impact. He tried to step back, to run away, but his legs refused to move. A whimper rose up, and he realized it came from him. Helpless, he glanced over to Tsubàyo in hopes that the older boy would do something Rutejìmo could follow.

Tsubàyo had a look of stunned shock on his face. Sweat glistened on his brow and his dull green eyes were shimmering. He started to turn away, but his gaze never wavered from Pidòhu's body.

Pidòhu moaned, a broken, wet noise. "H-Help…?"

Rutejìmo choked back sobs. He didn't know what to do. He knew he should rush over to Pidòhu, should have already done it, but he was afraid Karawàbi would stop him or berate him. Silently, he prayed Tsubàyo would step forward, turn around, or even give a command.

Pidòhu reached up from the shadows of the rock. His hand was bloody, and bits of sand and rock clung to his lacerated palm. "H-Help me…"

Rutejìmo looked back at Tsubàyo and then to Karawàbi.

Karawàbi seemed unfazed by Pidòhu's fall. Instead of shock or horror, the corner of his mouth had curled up in a smirk. He turned to see Rutejìmo looking at him and took a step toward him. "What? You have a problem?"

Whimpering, Rutejìmo stepped back. The sand swallowed his foot, and he almost lost his balance. He stumbled back, his eyes locked on Karawàbi, but the larger teenage boy didn't come any closer.

"I can see bone," sobbed Pidòhu, "and there's blood everywhere. Please, help?" His voice was ragged and gasping.

Rutejìmo felt sick to his stomach. He clutched his belly as he looked at Tsubàyo hopefully.

Tsubàyo spun on his heels and sprinted away from the accident.

Karawàbi chuckled dryly and dug his hands into his pockets. "Come on, Rutejìmo, it's time to go." And then he strolled after Tsubàyo while whistling cheerfully.

Rutejìmo turned back to Pidòhu but he couldn't force his legs to move. His mind screamed to run way with Tsubàyo, but something held him in place.

"Jìmo?" Pidòhu sobbed as he lifted his head. "Please help me."

Rutejìmo couldn't move.

"Where did Bàyo and Wàbi run to, Jìmo?" Chimípu's voice drifted through the wind behind him. She didn't seem concerned or worried; she must not have seen Pidòhu.

The urge to run away doubled, and Rutejìmo managed to get a foot out of the sand before setting it down heavily. He looked guilty at Chimípu as she walked up.

A few strands of her dark hair whipped in the wind that stirred the sands around her. A small plume of dust was dissipating behind her as if she had been running.

"Mípu?" called out Pidòhu.

In a flash, Chimípu's curiosity turned to concern and then to anger. She rushed forward. Rutejìmo was in the way, but she shoved him hard to the side. "Where is he!?"

Rutejìmo stumbled back, his eyes locked on her.

Her eyes widened and she gasped. "Dòhu!?" She sprinted over to him before dropping to her knees next to his prone form. "What happened!? Where are you hurt?" She gingerly felt along his arms, no doubt looking for more broken bones.

"I-I fell. And hurt myself."

"Tachìra's left nut. You didn't do this yourself." She glared at Rutejìmo. "What did you do to him!?"

Rutejìmo gulped, fighting the overwhelming desire to run away. He held up his hands. "I-I didn't…." He couldn't finish the sentence. He didn't do anything, but he could have. A thousand different things flashed through his mind, and the guilt tore at him.

Chimípu glared at him. He saw disgust and anger burning in her green eyes. She turned away. "Just… just, go away." She shifted to her other knee and started to inspect Pidòhu's wounds closely.

It was the hardest thing for Rutejìmo to step closer. It wasn't only curiosity that drove him, but an unfamiliar sense that he needed to do something. He didn't understand it, but seeing Pidòhu splayed out on the ground gave him a sick feeling. Still, as much as he wanted to run away, he couldn't do anything but creep closer.

Tears ran down Pidòhu's face. Shaking, he clutched Chimípu's thigh and dragged himself over until his head rested on her leg. There was a strange sense of familiarity between them as Chimípu stroked his hair and whispered softly. He looked up but said nothing.

Together, they lifted their heads to look down at Pidòhu's right leg. The jagged ends of both bones stuck out of his leg, the four cracked ends dripping with blood. The wound soaked the fabric of his trousers and dripped to the ground in a rapidly growing crimson stain.

Rutejìmo dropped to his knees as he threw up. The acidic taste burned in his mouth, and he wished it would burn away his memories.

"Jìmo!" snapped Chimípu.

Rutejìmo jerked and looked up guiltily.

"Get my pack," she ordered.

He was relieved to get an order and direction. Turning around, he willed himself to move and headed back the way Karawàbi and Tsubàyo had run.

"Hurry!" yelled Chimípu from behind him.

Jogging, he ran around the rocks. When he saw Tsubàyo pawing through Pidòhu's bag, he slowed to a stop. Pidòhu's supplies were spread out along the ground, carelessly tossed aside. Some of his clothes were already buried by the blowing wind.

Karawàbi stood near Tsubàyo, hands in his pockets and a cruel smile on his face. He had two packs slung over his shoulder, Tsubàyo's and his own. Their tents were already strapped to the side.

As Rutejìmo approached, Tsubàyo stood up. "How bad is it, Jìmo?" His voice was calm and calculating.

Rutejìmo swallowed back the bile. "Not good, Bàyo. He broke bones, and they are sticking out. I… I can see blood everywhere."

Tsubàyo reached into Pidòhu's pack and pulled out a small leather book. He looked at it briefly before throwing it over his shoulder. He grabbed the nearly empty bag before straightening back up. "Pity. We'll just have to go on without him. Come on and take this, I've gotten rid of the useless stuff." He held out the bag for Rutejìmo.

The sick feeling redoubled. Rutejìmo almost bent over as he felt it burning his throat, choking him. "I-I have to get Mípu's bag."

"Why?" Tsubàyo shook his head and held out his hands. "Dòhu isn't going to survive with a broken leg."

"B-Because, she… told me to." Rutejìmo couldn't look Tsubàyo in the eyes.

"You pathetic, worm-rotted pile of diseased crap. Are you incapable of doing anything on your own?"

Rutejìmo's cheeks burned at the insult. He felt ashamed for considering abandoning her but also for not following Tsubàyo. He didn't know which one he should obey.

"Jìmo!" Chimípu's call echoed off the rocks.

Rutejìmo gulped and grabbed her bag. "I have to get this."

"Go on, boy, run to your mistress." Tsubàyo kicked sand at Rutejìmo's face. "Like a good puppy."

Choking on his response, Rutejìmo dropped Chimípu's bag. He scrambled to pick it up before Karawàbi could step on it. His finger caught Pidòhu's leather book in the sand. After a moment's hesitation, he grabbed it and Chimípu's pack and backed away from the two.

Karawàbi snorted with disgust, and that was enough to spur Rutejìmo to run back to Chimípu and Pidòhu.

On the far side of the Wind's Tooth, Chimípu had stripped off her shirt and was using the fabric to blot the blood oozing out of the wound. Her dark skin glistened with sweat, except where she had a long, white cloth wrapped around her breasts. She glanced at Rutejìmo as he sank to the ground next to them.

"Took you long enough," she snapped.

"Sorry."

"Put your hand here." She pointed to a bloody spot on the cloth. "And put pressure on it. But, don't touch the bone itself."

Rutejìmo almost threw up again as he looked at the gaping wound. He clutched the book tighter.

"Now!"

Closing his eyes, he gingerly reached out to obey.

Chimípu grabbed his wrist and yanked him closer.

Rutejìmo lost his balance and shoved his hands forward. He slammed into the sand between Pidòhu's legs. Gasping, he opened his eyes to see himself only inches away from the bloody wound.

Chimípu yanked his hand from the ground and jammed it against the sticky, hot fabric. "Pressure, now. I won't lose Dòhu because you can't handle some blood."

She grabbed her pack and dug into it.

Underneath Rutejìmo's hand, Pidòhu shook violently. His face was pale and drawn.

Rutejìmo felt bones scraping as Pidòhu clenched and then let out a wail of pain.

"I'm here, Dòhu. And you'll be all right. Just give me a second." Chimípu's voice was compassionate and caring when she spoke to him. Not like the sharp orders she gave Rutejìmo. She let out a grunt of triumph as she pulled out a leather roll from her pack. With a snap of her wrist, she unrolled it along the ground to reveal a field kit. One end had a collection of knives, needles, and awls. The other had small pouches that would contain herbs, string, and other items used for wounds and repairs.

She looked up at Rutejìmo and Pidòhu. "Jìmo, use both hands and more pressure, it's leaking."

Rutejìmo looked down to see blood trickling from underneath the fabric. He gulped and sat down the leather book before gingerly pressing both hands on the blood-soaked fabric.

Chimípu pulled out two bunches of leaves. She crawled around Pidòhu, dragging the roll, and settled into place opposite Rutejìmo. Her hand was steady as she pressed the leaves to his lips. "Get a lot of saliva in your mouth, and suck on these. Don't swallow, though. These will help with the pain."

Pidòhu groaned. "I know, I helped you pick them, remember?" He opened his mouth and accepted the herbs.

She smiled weakly. Turning away, she started for the field kit but stopped sharply. "Jìmo, why did you bring that?"

Rutejìmo peered down to see that she was pointing to Pidòhu's book. "Um, Karawàbi is… going through Pidòhu's bag. They're leaving."

Chimípu stared at him incredulously. "Seriously? Here? Now? But what about Dòhu?"

He didn't want to say the words. He looked away from both her glare and the wound.

She jammed her finger into his side. "Don't they know he's hurt?"

He could only nod.

Chimípu stood up sharply. "What is wrong with all of you!? They can't leave. We can't split up the clan, not here, not now!" Her shrill voice echoed against the rock. She dropped the other leaves on Pidòhu's stomach. "Dòhu, tell the daft idiot how to pack the wound, I have to stop them."

Rutejìmo started to follow her as she ran after Tsubàyo and Karawàbi. When Pidòhu moaned, he turned back. "I-I… what do I do?"

Pidòhu pushed the sodden herbs into the side of his mouth before he answered. "Pull back the shirt and pack these"---he scooped the herbs up from his stomach---"where it is bleeding. It will stop the flow and prevent poisoning."

Looking down at the herbs, Rutejìmo shook his head. "Dòhu, I don't think I can do this."

Pidòhu said nothing, but there was a pale, frightened look on his face. Tears still ran down his cheeks. "It really hurts."

At the sound of Pidòhu's cracked voice, Rutejìmo sighed and wiped the tears from his eyes. "I'm sorry. I… I didn't… mean… "

"I know," whispered Pidòhu.

Gulping, Rutejìmo steeled himself. He tugged back the fabric. It clung to the wound, and he pulled harder. As it peeled off, a fresh spurt of blood blossomed in the gaping wound around the bone.

Pidòhu whimpered. "Hurry, please?"

Rutejìmo squished the leaves in his palm. Gulping to fight the bile rising in his throat, he steeled himself to his task. With a shaking hand, he gingerly shoved the herbs into the torn flesh.

Pidòhu hissed in pain and Rutejìmo yanked his hand back.

"No, keep doing that. Hurry."

Rutejìmo tried again and shoved the herbs into the wound, focusing on the areas that had blood pooled against the skin.

The injured boy let out a cry when Rutejìmo accidentally scraped against the ragged end of bone.

"It isn't working." Rutejìmo felt helpless, "Do I need more?"

"Just give it a few seconds," gasped Pidòhu. "Just a few seconds."

"There is so much blood." Rutejìmo hated that he was whimpering himself, but he couldn't move his gaze away from the blood filling the wound. The bare ends of the bones, four ragged spears of splintered white, twitched with every beat of Pidòhu's pulse.

Tenderly as he could, Rutejìmo dabbed at the blood with the corner of the fabric. The cloth quickly grew hot and sticky, but he hoped he was helping instead of making it worse.

Pidòhu let out a sudden gasp. "Oh, it's working."

Rutejìmo glanced down and realized the bloody wound was crimson, but blood no longer flowed out from the ragged ends of skin. He gulped at the nausea that slammed into him. "W-What do I do now?"

Pidòhu's eyes were glazed over as he glanced down. His cheek moved as he swirled the pain herbs in his mouth. With a nod, he pointed to one of Chimípu's shirts spilled out on the sand. It was her white ceremonial top, imported from the northern regions and very expensive.

"A-Are you sure?"

He nodded and moved the herbs to the side to speak. "It will soak it up. I'll buy her a new one, I promise."

"I---" Rutejìmo stopped speaking and grabbed it with one hand. He shook it free of sand, then carefully pushed it past the broken bone and into the wound. The bright white fabric turned crimson almost immediately.

Pidòhu jerked when Rutejìmo accidentally scraped the bone.

"I'm sorry!"

"J-Just hurry up, please? Don't move my leg, but try to staunch as much as you can."

Following Pidòhu's instructions, Rutejìmo carefully stuffed part of the shirt into the ragged wound. As he did, he could see how the sodden fabric helped bind the bones together and prevent them from ripping the skin further apart. When Pidòhu told him to wrap it tighter, Rutejìmo hesitated but obeyed. More blood oozed out of the wound, but Pidòhu gave a sigh of relief when Rutejìmo finished tying the limb and immobilizing it.

"Like that, Dòhu?"

When Pidòhu didn't answer, Rutejìmo looked up with concern. Pidòhu was slumped back, breathing shallowly, but his eyes were rolled into the back of his head.

Terrified Pidòhu had somehow died, Rutejìmo reached up and rested his hand on Pidòhu's chest. The rapid rise and fall gave him comfort. "I'm sorry, Dòhu."

The scrunch of sand tore his attention away from Pidòhu. He looked up as Chimípu dropped to her knees next to him. She pressed a hand against Pidòhu's throat and cocked her head.

Rutejìmo noticed she had a bruise forming over her eye and her knuckles were scraped. Part of her lip was also split, and she licked away the blood welling up.

Chimípu glanced at him, then back to Pidòhu. "Tsubàyo and Karawàbi are going their own way. If you want to go with them, I recommend you pack up and get the sands out of here."

Her voice was harsh and bitter. He trembled at the noise of it. He looked back at the rocks and then to Pidòhu. "Is he… going to live?"

"I will do everything I can to save him." She glared at him. "Unlike Tsubàyo and Karawàbi, I don't pick who is in my clan. That's Shimusògo's decision, not ours."

"I didn't…." But the words wouldn't come. He shivered from the intensity of Chimípu's look. Turning away, he swallowed to ease his tight throat. "I wasn't choosing."

"Dòhu?" Chimípu whispered, but it was soft-edged and compassionate. Rutejìmo glanced over to see her leaning over Pidòhu and stroking his sweat-soaked hair. "Just hold on, okay? As soon as the incontinent piles of horse crap"---her eyes flickered toward Rutejìmo---"go away, I'll get something for you to eat."

Rutejìmo got his feet under him, but didn't stand up. Crouching, he watched as Chimípu focused on Pidòhu. It felt as though she was just counting the seconds until he left. "Chimípu?"

Slowly, she turned toward him. "Make a choice, Rutejìmo, if you can. Either stay here and be useful or just leave."

As he stood up, the sick feeling grew in his stomach. It was a burn that threatened to rise up in his throat. He glanced down at his blood-stained hands. "Mípu… I'm sorry."

"Go away, Jìmo." She didn't look at him again. "Just go away."

He saw a tear roll down her bruised cheek before she turned away from him.
