---
when:
  start: 1471/3/46 MTR 20::8
date: 2012-09-08
title: An Evening Run
locations:
  primary:
    - Putsukami Dunes
    - Putsukami Bird Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Shimusògo
  referenced:
    - Kamanen
    - Mifúno
    - Tachìra
    - Hyonèku
    - Desòchu
organization:
  secondary:
    - Shimusògo
summary: >
  Finding shelter for the night, Chimípu made sure Pidòhu was safe and then asked Rutejìmo on a run. They ran together for some distance and then stopped, talking about what was going on. She revealed some details about Rutejìmo's brother. Then she taught him how to fire rocks at high speed using their speed magic.
---

> Every magic has a mechanism to activate. It could be precise rituals, prayer to a divine power, or dancing. --- Kamanen Porlin

Rutejìmo trudged forward, focusing on digging each foot into the shifting rocks and lurching to pull the sled after him. His back and legs screamed out in pain, the ache burning clear up to his shoulders. He couldn't feel his fingers anymore; they had stopped bending hours before, and he panicked when he first saw the claw-like curve to them.

Pidòhu's stretcher pulled against him, held down by weight and friction.

Rutejìmo let out a cry as he forced himself up, one step at a time. At the top of the dune was a rock with a broad shield against the sun and wind. It looked like a sand tick on the back of Mifúno, the desert, but it was shelter.

His eyes streamed with tears from the agony of pain and from staring into the burning red orb of the sun. It was sunset, and they had barely made a third of the route Rutejìmo had run the day before. It was painfully slow, which only made their efforts worse.

Chimípu jogged back down the sand, running along a shifting ridge. She held out her hands for the ends of the frame.

Rutejìmo shook his head and kept on trudging up.

"Damn it, Jìmo. Let me take the last rod. You've been dragging him for two hours now."

Rutejìmo gasped and shook his head again. His cracked lips worked silently for a moment. "You carried him through the high sun. For far longer than two hours."

"Yes, but I'm…." She closed her mouth.

"Better, I know. But I will"---he grunted and dragged himself farther---"do this!"

He expected her to shove him aside or to take the back end, but she didn't. Instead, she turned and walked next to him, keeping with his agonizingly slow pace as he dragged Pidòhu up the side of the dune and into the shade of the stone.

The shadows felt wrong to him, as if Tachìra's sunshine could no longer reach him. Frowning, he wiped the sweat from his brow and took a step back into the heated sun. The heat and light was a comfort, and he sat down heavily on the sand.

Pidòhu groaned and reached out for the rock above him. His fingernail scraped on the stone before he slumped back. "I like it. Homey." He said with a strained chuckled.

Rutejìmo smiled and stared down at his hands. The joints were locked in agony, curled around a handle that was no longer in his palms. He jammed his hands into the searing sand; the heat was nothing compared to the ache of his frozen joints. He flexed, wincing as he worked at loosening his fingers.

"Can you find water, Dòhu?" Chimípu's voice was just as broken as Rutejìmo's, exhausted. Her body was soaked in sweat, and the fabric of her shirt clung to her skin.

Pidòhu peered around, scanning the sands.

Rutejìmo watched with surprise. He knew the basics for finding water, but Pidòhu wasn't simply looking for the lowest place. Instead, he was searching for something specific.

Bracing himself against the rock, Pidòhu pointed to a low spot with a dark patch of sand. "There. About three feet down. There are some rocks too, to brace the sides."

"Thank you, Great Shimusogo Pidòhu." Chimípu bowed and grabbed a set of spikes and the translucent fabric used to gather moisture. She trotted down to where Pidòhu pointed.

Rutejìmo turned to the pale boy. "Why there?"

Pidòhu chuckled and then shivered. "There are shadows pooling there. It feels… cool."

Chimípu was kneeling in a brightly lit valley between two dunes. There was no shadow or obstruction. "Shadows…? Where?"

"Yeah, I'm seeing them everywhere. A flit there, a breeze there. They flash across the desert like some bird…" He pointed up "…sailing high up there."

Rutejìmo was about to change the topic when he saw Pidòhu's attention shift. The injured boy was watching something moving across the sands. He turned to look. From the corner of his eye, he caught a flash of movement. He spun around, but once again, there was nothing but sand and sun.

Pidòhu chuckled. "Shadows in the corner of your eyes?"

The world tilted as Rutejìmo stumbled. Trembling, he turned back to Pidòhu. "What?"

"That's what you see? Shadows on the edge? Always fleeing before you can focus on them?"

Rutejìmo squirmed. "Maybe?"

"You've always been a lousy liar, Rutejìmo. Don't worry, they are real. I can just see them better now."

"W-What are they?"

"I think I know." Pidòhu smiled. "But I'm not worthy to name it."

Before Rutejìmo could ask more questions, Chimípu came up. Her left hand was still curled in a hook from carrying the frame. "The collector is set up. We should have clean water come morning. How are we on skins?"

Pidòhu lifted one up. "Last one."

Chimípu looked around. Then she focused on Rutejìmo. "Dòhu?" she asked without moving her gaze from Rutejìmo. "Do you mind if we hunt for food?"

Rutejìmo's stomach lurched. He was suddenly afraid of the hard gaze fixing him in place.

Pidòhu shook his head. "No, Mípu. I'll be fine for now."

"We'll stay close," she said. Taking a step back, she beckoned for Rutejìmo.

Nervous, he got to his feet.

Chimípu gathered up some strips of cloth from the frame and a few rocks, wrapped them together, and then hoisted the bundle over her shoulder. She gave Rutejìmo a look that commanded him to follow.

He gulped.

She jogged along the ridge, heading toward a field of rocks and gravel.

Exhausted, Rutejìmo almost sat back down.

"Go on, Jìmo. Shimusògo run."

Rutejìmo gave Pidòhu a smile and then jogged after Chimípu. She was ahead of him, and he pushed to catch up. At first, his body was tight and unresponsive, but as the heat of jogging worked at his joints, he relaxed into the comfort of running.

Chimípu ran down the side of a ridge and along the base of a shallow valley.

Rutejìmo followed, pushing himself to run faster. Exhaustion tore at him, but anticipation grew as he raced. His body sang with joy as he felt the dépa arriving, a flicker of feather and a trail of prints.

And then it was ahead of him, a tiny bird speeding along the rocks with a streamer of dust behind it.

Grinning, Rutejìmo focused on catching up with the dépa. As he concentrated on running, the aches and pains melted away. The sun no longer burned his skin and his breath came easier. He accelerated, pulling closer to Chimípu.

The dépa sprinted forward until it was running in front of both of them. Chimípu didn't appear to respond to it, and he wondered if he was the only one who could see it. Pidòhu said that Chimípu had already felt Shimusògo, but he couldn't tell if that was true.

They came up to where the valley split in two. The dépa swerved to the side, but Chimípu was heading down the other fork.

Rutejìmo was torn over which one to follow, but then Chimípu veered after the dépa.

Her feet traced the bird's path too accurately not to see it.

Rutejìmo felt jealous but elated. He wasn't hallucinating. Biting his lip, he pushed his body to its limit. The ground blurred underneath him as his world became only three things: Rutejìmo, Chimípu, and the dépa.

With feet pounding on the rocks, he realized he was running side by side with Chimípu. There was no pain, no exhaustion. Just a liquid pleasure slipping through his veins. He was keeping up with her. Years of being in her shadow and somehow he was even with her.

He almost cried out in joy.

She shot him a glare, but there was the same excitement in her eyes. Their bodies were moving in sync, feet hitting the ground at the same time and the world blurring around them. Wind, hot and cool at the same time, whipped across his face.

The dépa came up on the end of a valley and then disappeared a few feet shy of a ridge top.

Rutejìmo and Chimípu ran past the spot it faded and to the top. When Rutejìmo started to crest over the top, he saw movement in the valley beyond. With a yelp, he tried to stop, but his body kept on moving. He lurched to the side and dug his feet in. The ground tore up around him, sand and rocks bursting in all directions as he carved a deep gouge into the gravel.

When he came to a stop, he was shaking. He stared back behind him, and there was a trench in the rock and sand a chain in length, sixty feet of his body tearing into the ground. He looked at his hand, expecting to see it bloody but it was unharmed. His feet were also unharmed, despite leaving a deep gouge in rock and gravel.

Next to him, Chimípu let out a yelp of joy as she stumbled out of a cloud of dust and sand. She also smiled broadly, her first real smile Rutejìmo had ever seen up close. Her eyes twinkled as she looked him over. Breathing softly, as if she hadn't just spent ten minutes sprinting, she looked him over. "You really can see Shimusògo, can't you? The dépa?"

Surprised, Rutejìmo let out a croaking noise.

She smiled and looked up to the sun, a quiet prayer to Tachìra moving her lips. When she spoke, it was a whisper. "It's a rush, isn't it?"

Rutejìmo gulped and glanced back at the ground. There were two long gouges in the earth from their stopping. He felt voiceless as he moved his mouth, but he couldn't form the words.

Chimípu glanced at him and then back at the gouges. "I haven't figured out how to stop either." She gave him another smile and he felt dizzy. "The others make it look so easy."

"I-I… how? How did you know?"

She gestured back the way they came. "I heard you last night."

He tensed up, a whimper rising in his throat. He hadn't thought she'd heard him being honest with Pidòhu.

Chimípu took a step to him. He cringed but she hugged him tightly, their bodies hot against each other. She stepped back after a heartbeat, her cheeks dark. She looked away, toward the horizon. "When my mother died… I was so upset. She spent a week dying in front of me, and there was nothing I could do in the end. I just"---tears welled up in her eyes---"started screaming."

She stared at the ground. "Hyonèku heard me and chased me out of my mother's cave and the valley. Made me run, but he wouldn't stop following me." She toed the ground, kicking rocks into the trench she had made while stopping. "I kept running and running. I thought I was going to pass out, but the pain wouldn't go away. Just when I was about to drop to my knees, I saw him. Shimusògo."

Rutejìmo thought about the dépa and how it drove him forward. He hadn't wanted to give up then and it came to him. He said, "I know, right when you are about to break."

When she looked up, there was a smile on her lips and tears in her eyes. "We are Shimusògo, aren't we?"

Rutejìmo gulped at the dryness in his throat. "I… I think so."

Chimípu nodded, then gestured to the valley they had almost entered.

Rutejìmo blushed when he realized why they stopped. In the valley below, there were thousands of nests. It was the nesting ground for rock fishers, a large bird that fed on cactuses and small insects. Feathers fluttered as the birds regarded them; a few took off when Rutejìmo stepped back.

"Dinner," Chimípu said.

"You going to catch them?"

She shook her head. "No, I need practice throwing."

"Throwing?" Rutejìmo frowned.

He watched as Chimípu took hold of the rock and cloth she had brought with her. She formed a makeshift sling and held both ends of the fabric. "Great Shimusogo Desòchu showed this to me once as a girl. After seeing Shimusògo, I finally figured out how to do it." She gave him a sheepish smile. "Though I'm not very good yet. Step back."

Rutejìmo obeyed and leaned against a rock.

Chimípu planted her feet and held the fabric with both hands. Taking a deep breath, she spun around in full circle, then another. Her feet stomped on the same spot as she spun. The sling swung out from her, and Rutejìmo cringed in fear that she would drop it.

But she kept spinning. He was expecting her to fall down with dizziness, but she pushed harder.

And then the dépa was there, sprinting in a circle around Chimípu. Her movements accelerated, and a vortex of dust rose up around her, blurring her form. The sling formed a disk as it spun with her.

She stopped suddenly and released one end of the cloth. The rock shot out and crossed the valley in an instant. It left ripples in the air as raw power rolled off the stone. A crack of air shook Rutejìmo from its passing. The stone shattered at the base of a nest and there was a shower of blood and feathers.

Rutejìmo gasped and stared in shock. "Drown me in sands! My brother can do that?"

Panting, Chimípu grinned at him and nodded. "The hunting bolas are a lot more accurate, and deadlier. Your brother says that a warrior can break a neck a mile away with one of them."

"I… he never showed me that."

She hefted a rock. "Want to try?"
