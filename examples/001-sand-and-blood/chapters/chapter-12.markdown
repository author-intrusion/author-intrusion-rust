---
when:
  start: 1471/3/44 MTR 3::57
date: 2012-08-25
title: Investigating the Night
locations:
  primary:
    - Nisanto Finger Stop
  referenced:
    - Wamifuko City
    - Shimusogo Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Karawàbi
    - Tsubàyo
  referenced:
    - Kalem
    - Pidòhu
    - Chimípu
organizations:
  secondary:
    - Metokāchyu # Scorpion builders
    - Shimusògo
    - Pabinkúe
topics:
  referenced:
    - Primitive Legends of the Mifúno Desert
    - Shimusogo Way
summary: >
  Rutejìmo set up the camp and cooked food for the other two teenagers. Tsubàyo and Karawàbi also left the cleaning to him. As it grew dark, they noticed campfires in the distance. Tsubàyo was curious and decided to investigate. Karawàbi remained behind, but Tsubàyo made Rutejìmo come along.

  Once they got closer, they saw it was a caravan with some strange brass item. The caravan had a small herd of black horses and Tsubàyo decided to steal some. Rutejìmo refused, claiming that the Shimusògo's power came from running, not riding. They argued before deciding to go their separate ways.

  Rutejìmo turned and headed back toward their shelter, thought it was impossible to find in the darkness.
---

> The desert folk have a quaint legend that the sun, as personified by the spirit Tachìra, is in constant rivalry with the moon, named Chobìre, for the affections of the desert, Mifúno. --- Kalem Ratenbur, *Primitive Legends of the Mifúno Desert*

As the sun sank below the horizon, darkness flooded the camp. Rutejìmo pulled out a handful of glow eggs from the various bags and set them up around the camp. Each egg was carved out of rock with a glass globe blown inside. Inside, there was a clockwork mechanism attached to a metal spike poised over a crystal mounted in the middle. Rutejìmo wound a key in the bottom and released it. The spike began to tap rapidly on the crystal which created a tiny flash of bluish light. It flickered in the corner of his vision, but it was sufficient for his remaining duties.

Neither Tsubàyo nor Karawàbi helped pitch the tents, but Rutejìmo wasn't expecting them to. It was clear he was the weakest member in the group of three and it was his duty to do the servile tasks. It rankled, though, and he wondered if Pidòhu felt the same thing when forced to make dinner for the other clan members.

He dragged his bag and Pidòhu's to his tent. He tied the flaps shut before picking up the abandoned plates and heading some distance from the campsite to clean them. Without water, he used a handful of coarse sand to scrape the plates clean. Then, he found a patch of finer sand to remove the sweat and grime off his own face and chest. It wasn't a satisfying clean, but it would keep him for the night.

As he cleaned himself off, he pinned the glow egg against his bare chest with his chin and kept his back to the other clan's campsite. There was almost no chance that anyone could see such a dim light from such a distance, but the desert was filled with clans possessing remarkable talents. Just as his could run with speed, there were others who commanded fire, horses, or even rock.

Duties finished, Rutejìmo was ready for bed. He hoped tomorrow would help ease the guilt tearing into him and the feeling that he had made a mistake. He wrapped a leather strip around the plates for storage and headed back.

"Jìmo?" Tsubàyo stopped him as Rutejìmo was walking past.

"Yes… Great Shimusogo Tsubàyo?" The muscles in his chest tightened.

"Why don't you, Karawàbi, and I go check out that clan?"

Rutejìmo looked at the fires in the distance. "I don't think…." He gulped as a feeling of discomfort rose inside him. "I don't think we should do that."

"Why not?"

"It's night. We can barely see. And what if they are clans of the moon? We could be in danger by attracting their attention."

Tsubàyo rolled his eyes. "It is the middle of the night. For all the moon crap, I really doubt they can see in the dark. Just a check. And maybe see if we can get supplies to head home."

"You mean buy? We don't have a lot of money."

"Yeah, sure, I meant buy. Come on, they are only a mile or so away. It will take us an hour, tops."

"A mile in pitch-dark, Bàyo." At Tsubàyo's glare, Rutejìmo corrected himself. "Great Shimusogo Tsubàyo."

"Well," Tsubàyo said as he stood up. "We're going."

Karawàbi grunted. "I'm staying."

Turning on his friend with a glare, Tsubàyo snapped out, "What?"

"I'm tired. I'm going to sleep." Karawàbi yawned as he staggered to his feet.

"Fine! It will just be me and Jìmo."

Rutejìmo gestured to the tents. "Can't I stay?"

"No."

"Why can Karawàbi stay but I can't?"

"Because he isn't a shit. Come on, boy."

Rutejìmo glanced helplessly at Karawàbi, hoping for help.

The larger teenager glanced at him, then made a shooing gesture with his hand.

Dismissed, Rutejìmo looked around before he headed to his tent.

"Where are you going, boy?"

"To get my pack."

"Why?"

"We always carry our packs. It is the Shimusogo Way."

"Well," Tsubàyo said in irritation, "leave it. Karawàbi will watch it, and we'll be right back."

"But---"

"I said leave it. Come on, damn it, or I will have Karawàbi beat the crap out of you."

Rutejìmo gulped and glanced at Karawàbi, who had frozen as he crawled into his tent. The larger boy stuck his head out, and there was a cruel hope in his eyes.

Sick to his stomach, Rutejìmo shook his head. "I'm coming."

Karawàbi looked disappointed, and Rutejìmo let out a sigh of relief.

Rutejìmo hated himself as he turned and followed after Tsubàyo.

The walk to the other clan was long. Tsubàyo and Rutejìmo strode in a small pool of blue light from the glow eggs. Beyond a few feet in either direction, the pitch-darkness obscured everything. There were no landmarks, no dunes to look at. Rutejìmo couldn't prepare for the shifting sands or rocks until he was right on it. More than a few times, he tripped when he missed a dip or ridge that would have been obvious in the sun.

It was also cold in the darkness. The sand, while baking hot in the day, quickly lost heat. Within a few minutes of leaving, Rutejìmo's breath fogged in the air, and the tapping of his glow egg was only slightly faster than the occasional chatter of his teeth.

As they got closer, it was obvious that the other clan wasn't worried about anyone seeing them. At least a hundred people moved between silk pavilions, smaller tents, and sand sleds. The fires drew Rutejìmo's attention. The largest was in the center, and the roaring flames cast light and heat into a reddish haze. Four nearly naked people, three males and one female, transferred large logs from a wagon to the bonfire, tossing each one at a steady rate. The light made their dark skins look black.

There were eight other fires in the camp. They were smaller, but also fed by a constant supply of wood. Unlike the bonfire, each of the smaller fires were built up around a brass column that reached out of the light from the fires. The base of each column was covered with letters and names, but he couldn't read any of them from his position.

"Bàyo? What are those things for?"

Even in the dim light, Tsubàyo's glare was obvious.

Impatient and annoyed, Rutejìmo corrected himself. "Fine, Great Shimusògo Tsubàyo, what in the sand-cursed winds are those columns for?"

"Probably something for the spirit. I don't care. Look"---he pointed to the far side of the camp---"they have horses."

Rutejìmo looked in the same direction. Horses milled in a herd a few chains from the rest of the camp. He had missed seeing them since they weren't near one of the strange columns or fires. Instead, their black bodies were almost invisible in the darkness except for the green reflections of their eyes. He shivered at the sight of them; black horses were one sign of the night clans.

Even though there were easily two dozen horses, there were only two clan members attending them. Both were slender and short and female. They said nothing as they ate from wooden boards while perched on a rock. One looked out over the horses, and the other watched the people.

Rutejìmo leaned into Tsubàyo. "So? We are Shimusògo, we don't need horses."

"They would make it easier to head home," said Tsubàyo. He licked his lips as his eyes glittered. "And there is only one of them watching. We can take both of them."

"They are probably horse lords and don't need to see their horses. What do you think you'll do, just wander over and purchase three of them? I don't know about you, Great Shimusògo Tsubàyo, but I left my riches at home."

Tsubàyo grinned. "We'll steal them! And then ride away from all of this crap."

Rutejìmo interposed himself between Tsubàyo and the horses. "Are you insane!?" he whispered sharply. "Do you know what they do to people who steal horses? They gut them, attach their intestines to a horse, and yank. I don't know about you, but I really don't want to see how long my guts stretch across the sand!" His voice threatened to rise above a loud whisper, but Rutejìmo couldn't help it.

Tsubàyo's eyes glittered as he glared at him. "I know what I'm doing, boy. And I'm in charge. I say we steal them."

"No," Rutejìmo said with a shake of his head. "No, I won't ride a horse."

"Then we'll only have to steal two of them."

Rutejìmo continued to shake his head. He stepped away from Tsubàyo, the sick feeling rising in his throat. "No. I'm Shimusògo, and we don't ride."

Tsubàyo glared at him and whispered loudly, "Shimusògo abandoned us. The clan left us in the middle of the night. I don't really care what they want or what they say. I'm going to take those horses and head straight for Wamifuko City."

"I thought we were heading home."

"Curse them to sands." Tsubàyo pushed Rutejìmo aside and headed toward the horses. "Are you coming?"

"N-No." He gulped and repeated himself. "No." He felt a little better than the last time.

Tsubàyo's eyes glittered, and there was a knife in his hand. "You better not be in camp when I get back, boy. If you are, I'll kill you."

Rutejìmo stared at the blade in surprise. "Where did you get that?"

"I've always had it."

"You'd pull a knife on clan?"

"A clan that abandons us is no clan for me." Tsubàyo's voice was a growl. "Go on, head back. Go back to Mípu and Dòhu. I'm sure their weak spines will match your own." He stepped back toward the horses. "And we better not meet again, Jìmo, or you'll regret it."

Rutejìmo wanted to stop him, to try convincing Tsubàyo not to steal, but as he reached out for his clan brother, a yell rose up from the camps. Terrified that someone had caught them, Rutejìmo spun around and looked back at the camp with a sinking feeling.

One of the columns was crumbling in the fire. Clan members yelled out frantically as half of them ran away from the fire and the other half sprinted toward it. With sick fascination, Rutejìmo watched as the metal snapped and the column folded in half.

But, instead of simply falling to the side, a second part of the column fell out of the darkness and slammed into the ground. The other seven columns suddenly tilted toward the fallen one, and the sky above the bonfire lit up.

Rutejìmo held his breath as he stared at a large, curved glow of something towering over the massive fire. It stretched across all of the fires, and Rutejìmo realized that the columns were actually legs to some mechanical creature.

The yelling continued as people ripped the tents from the ground and dragged them away. Others grabbed lights and ran toward the darker parts of the camp. A few second later, they brought out a ladder. They set it against the side of the mechanical creature and quickly climbed the rungs.

Rutejìmo lifted his head as he watched them reach the bottom of the massive vehicle and crawl over it like flies on a corpse. The lights they carried were tiny motes against the brass-and-steel surface, but he could clearly see two large pincers and a curved tail reaching high into the darkness.

It was a scorpion and larger than anything Rutejìmo had ever seen. It would have dwarfed the Shimusogo Valley and would have reached his grandmother's cave even if it stood on the floor of the valley.

Something began to quake inside him. He was frightened, more scared than he had ever been. It was too large, too terrifying for his mind to comprehend. He looked around for Tsubàyo, but the other teenager was gone. Rutejìmo didn't know if he had headed back to their camp or for the horses. He didn't know anything but the fear that clutched his belly.

A groan of metal pounded in the air. A whimper rose in Rutejìmo's throat as he watched the mechanical scorpion come to life. Steam poured out of the crumbled leg, but the others gained a sudden rigidity, and the vehicle straightened as the seven feet sank into the ground.

Ruby light flashed across the desert. Rutejìmo flinched, but it wasn't an attack. He stared up at the vehicle and saw that it now had five glowing eyes on the side. One of the eyes was much larger, and there was no doubt there was a matching set on the far side.

Rutejìmo backed away from the chaos. He couldn't stay. As he stared wide-eyed at the people running around, he saw a woman leading a line of horses to the wood wagons. It was one of the two who watched over the horses. She was wearing a dark outfit, much different from the others. A curved blade flashed at her side. Her arms and back were bared, but almost every bit of her skin was covered in black tattoos.

The fear peaked, and Rutejìmo turned on his heels. He sprinted away, heading back to the camp. He didn't want to be found when they pulled the tents and pavilions away from the massive scorpion, nor did he want to be near it in case it fell to the ground.

The din quieted quickly as he ran blindly into the darkness. The glow egg gave only the faintest of lights, and he slammed into the side of a rock. Pain tore through his senses, and he spun as he hit the ground. Sobbing underneath his breath, he crawled away from the noise and light.

An earthquake ran through the sand. With grains pouring across his hands, he glanced back in fear that the scorpion was following. But the massive vehicle was stepping away from the fires and settling down with the crumpled leg stretched out across the sand.

Rutejìmo turned back and scrambled to his feet. He ran ahead, wishing that the dépa would return so he could follow it. Unfortunately, it was his own plodding limbs that dragged him across the sand and into the choking darkness of an unlit desert.
