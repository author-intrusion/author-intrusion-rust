---
availability: public
when:
  start: 1471/3/28 MTR 4::22
  duration: 0::30
date: 2012-02-18
title: Rutejìmo
locations:
  primary:
    - Shimusogo Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Hyonèku
  referenced:
    - Ganósho
    - Gemènyo
    - Chimípu
    - Yutsupazéso
    - Tejíko
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - The Wait in the Valleys
purpose:
  - Introduce Rutejìmo
  - Introduce Hyonèku
  - Introduce naming conventions
  - Introduce formality rules
  - Introduce the basic rules of politeness
summary: >
  Rutejìmo was on top of the clan's shrine roof trying to sneak in and steal his grandfather's ashes. It was a teenage game, but also one to prove that he was capable of becoming an adult. He ended up falling off the roof.

  The shrine guard, Hyonèku, caught him before he hurt himself. After a few humiliating comments, he gave Rutejìmo a choice: tell the clan elder or tell his grandmother. Neither choice was good, but Rutejìmo decided to tell his grandmother.
---

> When a child is waiting to become an adult, they are subtly encouraged to prove themselves ready for the rites of passage. In public, however, they are to remain patient and respectful. --- Funikogo Ganóshyo, *The Wait in the Valleys*

Rutejìmo's heart slammed against his ribs as he held himself still. The cool desert wind blew across his face, teasing his short, dark hair. In the night, his brown skin was lost to the shadows, but he would be exposed if anyone shone a lantern toward the top of the small building. Fortunately, the shrine house was at the southern end of the Shimusogo Valley, the clan's ancestral home, and very few of the clan went there except for meetings and prayers.

He held his breath as he tested the brick tile on the shrine-house roof. It shifted underneath his bare toe and he stepped back. Braced on both hands and one foot, he tested the second brick. It held and he eased his weight onto it before lifting his other foot. He was light and thin, slightly over five stone, and thankful of that as he adjusted his balance. He glanced up to his destination, an opening in the roof to let out smoke and incense. It was only a few inches beyond his fingers, but he didn't dare jump for it.

Bringing his weight to his forward foot, he walked his hands along the tiles until he found two handholds that were more stable. Inching forward, he stretched his foot and tested the next tile. It was solid and he leaned to put more weight on it.

A loud crack shot out and he almost lost his balance when his footing sank an inch. He let out a cry, but then bit down on his tongue to avoid alerting the guard inside. A flash of pain stole his breath away. He held his breath and waited for it to subside into a dull throb.

As he waited, he listened for the guard. If it was Gemènyo, he would just be sent back to his home. But, if Hyonèku was on duty he would be suffering for days. His stomach knotted in fear, and he listened for the telltale blast of air that always followed when anyone in the clan used magic.

A sand fly landed on his neck, its little legs pricking his skin. He tensed as he fought back a whimper. Sand flies bit when disturbed. He tried to lean forward, avoiding the tile, to encourage it to fly off, but it just crawled up to his earlobe.

Another fly landed on his shoulder. He caught sight of it in the corner of his vision, its black eyes illuminated by the dim light spearing up from the opening. It fluttered its wings as it crawled along, looking for some delicate spot to bite.

He forgot about the first fly until it bit down. The sharp pain broke his concentration, and he let out a yelp. He clapped his ear but missed the insect.

The cracked tile slipped again, spreading apart. His foot, resting along the crack, twisted as the tile shattered and he lost his balance.

"Sands!" he screamed as he slipped down the sloped roof. His back crushed another tile before he rolled off. He tumbled in the air and saw the earth rushing up to him. Closing his eyes, he threw his hands in front of his face to protect himself.

A blast of wind slammed into him a heartbeat before he fell into a pair of muscular arms. The wind howled around him, quickly dying before Rutejìmo could finish slumping into the man who caught him. From the flowery scent that remained, it was Hyonèku who had caught him. His wife had a distinctive perfume.

"Damn the sands," muttered Rutejìmo as he looked up into the face of his rescuer.

Hyonèku was almost six feet tall, with the wiry build that all Shimusògo shared. He had a short-cropped beard, but the hairs were still as black as the night. In the light from the shrine, his green eyes glittered.

"What were you doing, boy?"

Rutejìmo cleared his throat and wished he was anywhere else. He tried to reach for the ground but Hyonèku refused to set him down.

"I asked a question," said the older man.

"I was just looking." It sounded pathetic when he said it, and he could feel the arms holding him tighten.

"You were trying to steal a vase, weren't you? You're seventeen years old, not twelve."

Rutejìmo turned away. It was exactly what he wanted to do. Inside the shrine house were hundreds of vases, each filled with the ashes from one of the fallen clan. The plan was to steal his great-grandfather's vase and bring it to the entrance of the valley. It was an impromptu test of skill, speed, and stealth. From what he heard, Chimípu had done it twice, once to steal her great-aunt and once for her twice-great-grandmother. Both times, she left the vase on the threshold for the guards to pick up in the morning. She didn't have to say anything, but everyone knew she did it.

Her accomplishments rankled Rutejìmo; he hated that Chimípu did everything better than him. It wasn't fair. She was only a year older. Just because her father was the greatest warrior in the clan, she was given freedoms he could never enjoy.

Hyonèku set him down. "You're an idiot."

"Yes, Great Shimusogo Hyonèku."

In the language of the desert, being polite not only required a deferential tone but also using someone's full name, with the clan coming before the given name. Only the last part, the given name, was spoken with an accent to indicate gender. He knew there was nothing he could say to prevent his punishment, but he hoped a proper tone would help defer the worst of it.

"Come on," Hyonèku said as he gestured toward the path back to the rest of the valley, "I have to tell Yutsupazéso."

Yutsupazéso was the oldest of the clan members in the valley. She was also a dour and angry woman who delighted in making Rutejìmo's life difficult.

Rutejìmo's eyes widened. "Please don't tell her, Great Shimusogo Hyonèku. Anything but her. I promise I won't try it again. She made me clean out the fire pit last week! It took me four days!"

Hyonèku chuckled. "You did dump a pot of soup while roughhousing."

"It was an accident."

Hyonèku shifted the bandoleer of throwing knives to his other shoulder. "And was climbing on the shrine roof an accident too? Maybe you fell on the tiles?"

"No." Rutejìmo sighed and stared at the ground. "It wasn't an accident."

"Then you'll be doing chores until your hair turns gray and your legs wither."

Rutejìmo whimpered. He stepped back from Hyonèku, but froze when the elder glared at him.

Hyonèku turned his head to follow Rutejìmo, his green eyes shining in the dim light. "And what do you think I should do, boy?"

"Um, let me go?"

Hyonèku laughed, a loud, booming noise. Rutejìmo winced at the sound, worried it would carry down the valley. "Let you go? You just tried to break into the sacred shrine. I should have cut you down the second I heard you climbing on the roof." He turned toward Rutejìmo. "Or let you hit the ground."

Rutejìmo bowed his head again. "I'm sorry, Great Shimusogo Hyonèku."

"You should be." There was a pause. "Besides, you should have climbed up from the other side."

Rutejìmo gasped. He looked up to see Hyonèku smiling at him.

"The tiles here are fancy, but fragile. The back of the shrine is built with solid brick. Nothing to crumble or crack. Of course, if you had figured that out," his voice grew tense, "you'd have made some other stupid mistake, and I would have had my knife at your throat while you pissed your trousers."

Trembling, Rutejìmo forced his gaze back to the ground. He couldn't tell if Hyonèku was being generous or threatening.

The older man grunted and toyed with his knives. "All right, the elder doesn't have to know."

Rutejìmo looked up thankfully. He started to say something, but Hyonèku held out his finger.

"But you must tell your grandmother."

Stepping back, Rutejìmo held up his hands. "No, anything but her."

"Yutsupazéso then?"

"I-I can't tell any of them."

"Funny, you say that as though you have a choice," Hyonèku said without a smile. "If you don't tell either, I'll make sure to tell both."

Rutejìmo thought furiously, trying to figure out the lesser of two evils. As much as he feared his grandmother, he dreaded the clan elder more. "I will tell my grandmother, Great Shimusogo Hyonèku."

Hyonèku nodded and gestured down the path. "Then I will ask her in the morning how she dealt with you. Until then be safe, boy."

Rutejìmo sighed. He had to tell her now. Keeping his hands clasped together, he sullenly headed down the trail.

"Don't walk, boy, run. Run like you belong to Shimusògo."

He ran.
