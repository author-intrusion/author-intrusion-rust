use ai_cmd::CommandContext;
use ai_core::project::fs::get_project;
use ai_core::project::*;
use clap::ArgMatches;
use failure::*;

/// Parses the command-line options and pulls out the project-specific
/// elements and encapsulates them into a `Project` object which is
/// used to handle projects.
pub fn parse_cli(
    command_context: &CommandContext,
    matches: &ArgMatches,
) -> Result<Project, Error>
{
    let project_path = &matches.value_of("project");
    let project_path = match project_path
    {
        Some(path) => Some(String::from(*path)),
        None => None,
    };
    let project = get_project(&command_context.cwd, project_path)?;

    Ok(project)
}
