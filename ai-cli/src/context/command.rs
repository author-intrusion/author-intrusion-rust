use crate::args::ArgMatchesExt;
use ai_cmd::CommandContext;
use clap::*;
use failure;
use fern::{
    colors::{Color, ColoredLevelConfig},
    Dispatch,
};
use log::*;
use std::{env, fs::*, path::*, result::Result};

/// Parses the CLI arguments and create a `CommandContext` that represents
/// the common options used. This also creates the various handles and
/// structures used for working with projects.
pub fn parse_cli(matches: &ArgMatches)
    -> Result<CommandContext, failure::Error>
{
    // Set up the logging.
    setup_logging(matches)?;

    // All commands have a working directory, figure that out. If one is
    // provided, we use that as the path. Otherwise, we use the working
    // directory for the paths.
    let cwd = match &matches.subcommand_value_of("cwd")
    {
        None => env::current_dir().unwrap(),
        Some(c) =>
        {
            debug!("provided current working directory: {:?}", c);
            canonicalize(PathBuf::from(&c.to_string())).ok().unwrap()
        }
    };

    debug!("using working directory: {:?}", cwd);

    // Create the structure for the process.
    Ok(CommandContext { cwd })
}

fn setup_logging(matches: &ArgMatches) -> Result<(), failure::Error>
{
    // Pull out the options from the arguments.
    let verbosity = &matches.subcommand_occurrences_of("verbose");

    // Figure out the argument options.
    let level = match verbosity
    {
        0 => LevelFilter::Warn,
        1 => LevelFilter::Info,
        2 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    // Set up the colors.
    let colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Blue)
        .debug(Color::Cyan);

    // Set up logging with pretty colors.
    let mut logger = Dispatch::new();

    // If we are logging to the file, then we write out everything. Otherwise,
    // we write it out to the console.
    if let Some(log_path) = &matches.subcommand_value_of("log")
    {
        let log_file = fern::log_file(log_path)?;
        logger = logger
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "[{level:5} {target}] {message}",
                    level = record.level(),
                    target = record.target(),
                    message = message,
                ))
            })
            .level(level)
            .chain(log_file);
    }
    else
    {
        logger = logger
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "{color_line}[{level:5} {target}]\x1B[0m {message}",
                    color_line = format_args!(
                        "\x1B[{}m",
                        colors.get_color(&record.level()).to_fg_str()
                    ),
                    level = record.level(),
                    target = record.target(),
                    message = message,
                ))
            })
            .level(level)
            .chain(std::io::stderr());
    }

    // Finish by setting up the logger.
    logger.apply()?;

    Ok(())
}
