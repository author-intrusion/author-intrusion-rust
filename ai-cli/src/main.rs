use failure::*;
use log::*;
use std::process::*;

pub mod args;
pub mod cmds;
pub mod context;

fn main()
{
    // We display the final results using debug! so we can have clean output
    // unless the user asks spcifically for results.
    exit(match cmds::run_cli()
    {
        Ok(result) =>
        {
            debug!("CLI ended successfully: {:?}", result);
            result.exit_code
        }
        Err(err) =>
        {
            error!("CLI ended in failure: {:?}", err);
            1
        }
    });
}

/// Defines the common errors returned by the CLI process.
#[derive(Debug, Fail)]
#[fail(display = "there was a CLI error: {}", message)]
pub struct CliError
{
    message: String,
}
