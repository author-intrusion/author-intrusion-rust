use ai_cmd::content::list::ListContentCommand;
use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use ai_core::project::*;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(
    context: CommandContext,
    project: Project,
    matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    let fields = match matches.values_of("fields")
    {
        None => vec![
            String::from("file.rel_path"),
            String::from("file.num"),
            String::from("title"),
            String::from("count.words"),
        ],
        Some(v) => v.map(std::string::ToString::to_string).collect(),
    };
    let command = ListContentCommand {
        context,
        project,
        fields,
    };

    command.run()
}
