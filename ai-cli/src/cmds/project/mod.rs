use super::help;
use crate::context;
use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub mod find;

/// Parses the top-level project commands.
pub fn run_cli(
    command_context: CommandContext,
    matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    // All project commands also have a project context which contains
    // information about the current project.
    let project_context =
        context::project::parse_cli(&command_context, &matches)?;

    // We have to figure out the innermost subcommand and then unwrap it going
    // up through the levels.
    match &matches.subcommand
    {
        None => help::run_cli(&matches),
        Some(sc) => match sc.name.trim()
        {
            "find" =>
            {
                find::run_cli(command_context, project_context, &sc.matches)
            }
            &_ => help::run_cli(&sc.matches),
        },
    }
}
