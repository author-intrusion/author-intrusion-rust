use ai_cmd::project::find::FindProjectCommand;
use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use ai_core::project::*;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(
    context: CommandContext,
    project: Project,
    _matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    let command = FindProjectCommand { context, project };

    command.run()
}
