use ai_cmd::result::CommandResult;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(matches: &ArgMatches) -> Result<CommandResult, failure::Error>
{
    let message = match &matches.usage
    {
        None => "Unknown command.".to_string(),
        Some(u) => u.to_string(),
    };

    println!("{}", message);

    Ok(CommandResult::success())
}
