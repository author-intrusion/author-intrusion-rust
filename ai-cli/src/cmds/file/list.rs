use ai_cmd::file::list::ListFileCommand;
use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use ai_core::project::*;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(
    context: CommandContext,
    project: Project,
    matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    let fields = match matches.values_of("fields")
    {
        None => vec![
            String::from("file.file_type"),
            String::from("file.bytes"),
            String::from("file.rel_path"),
        ],
        Some(v) => v.map(String::from).collect(),
    };
    let command = ListFileCommand {
        context,
        project,
        fields,
    };

    command.run()
}
