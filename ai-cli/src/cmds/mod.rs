use crate::context;
use ai_cmd::result::CommandResult;
use clap::*;
use failure;
use std::result::Result;

pub mod check;
pub mod content;
pub mod file;
pub mod help;
pub mod lsp;
pub mod project;

/// Parses the command-line options and generates a populated `Command` object that represents
/// the desired functionality.
pub fn run_cli() -> Result<CommandResult, failure::Error>
{
    // Load the CLI options from the YAML and then parse the executables arguments against it.
    let yaml = load_yaml!("../cli.yml");
    let app = App::from_yaml(yaml).version(crate_version!());
    let matches = app.get_matches();

    // Create a context for the command. This figures out the working directory
    // and sets up logging for processing.
    let context = context::command::parse_cli(&matches)?;

    // I'm overly fond of subcommands (too much life with Git) so everything in
    // this application is arranged with nested subcommands. This is the match
    // statement that distributes the code to the appropriate and isolated
    // command. The structure of the modules matches the commands.
    match &matches.subcommand
    {
        None => help::run_cli(&matches),
        Some(sc) => match sc.name.trim()
        {
            "check" => check::run_cli(context, &sc.matches),
            "content" => content::run_cli(context, &sc.matches),
            "file" => file::run_cli(context, &sc.matches),
            "lsp" => lsp::run_cli(context, &sc.matches),
            "project" => project::run_cli(context, &sc.matches),
            &_ => help::run_cli(&sc.matches),
        },
    }
}
