use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use ai_lsp::stdio::StdioLanguageServerCommand;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(
    context: CommandContext,
    _matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    let command = StdioLanguageServerCommand { context };

    command.run()
}
