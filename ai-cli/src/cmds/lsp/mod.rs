use super::help;
use ai_cmd::result::CommandResult;
use ai_cmd::CommandContext;
use clap::ArgMatches;
use failure;
use std::result::Result;

pub mod stdio;

pub fn run_cli(
    command_context: CommandContext,
    matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    match &matches.subcommand
    {
        None => help::run_cli(&matches),
        Some(sc) => match sc.name.trim()
        {
            "stdio" => stdio::run_cli(command_context, &sc.matches),
            &_ => help::run_cli(&sc.matches),
        },
    }
}
