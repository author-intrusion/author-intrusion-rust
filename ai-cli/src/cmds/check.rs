use ai_cmd::{check::CheckCommand, result::CommandResult, CommandContext};
use clap::ArgMatches;
use failure;
use std::result::Result;

pub fn run_cli(
    context: CommandContext,
    matches: &ArgMatches,
) -> Result<CommandResult, failure::Error>
{
    // All project commands also have a project context which contains
    // information about the current project.
    let project = crate::context::project::parse_cli(&context, &matches)?;

    // Pull out the other options.
    let report_absolute_paths = matches.is_present("absolute_paths");

    // Run the command and return the results.
    let command = CheckCommand {
        context,
        project,
        report_absolute_paths,
    };

    command.run()
}
