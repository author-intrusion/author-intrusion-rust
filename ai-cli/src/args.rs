use clap::ArgMatches;

pub trait ArgMatchesExt<'a>
{
    fn subcommand_occurrences_of(&self, s: &str) -> u64;
    fn subcommand_value_of(&self, s: &str) -> Option<&str>;
}

impl<'a> ArgMatchesExt<'a> for ArgMatches<'a>
{
    fn subcommand_occurrences_of(&self, name: &str) -> u64
    {
        let current = &self.occurrences_of(name);

        match &self.subcommand
        {
            None => *current,
            Some(subcommand) =>
            {
                current + subcommand.matches.subcommand_occurrences_of(name)
            }
        }
    }

    fn subcommand_value_of(&self, name: &str) -> Option<&str>
    {
        let current = &self.value_of(name);

        match current
        {
            Some(p) => Some(p),
            None => match &self.subcommand
            {
                None => *current,
                Some(subcommand) =>
                {
                    subcommand.matches.subcommand_value_of(name)
                }
            },
        }
    }
}
