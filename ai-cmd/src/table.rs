use ai_core::config::project_config::ProjectConfig;
use ai_core::project::file::File as ProjectFile;
use num_format::{SystemLocale, ToFormattedString};
use prettytable::format::Alignment;
use prettytable::Cell;
use prettytable::*;
use serde_json::Value;
use std::sync::Arc;

pub fn write_file_list_table(
    config: &ProjectConfig,
    files: Vec<Arc<ProjectFile>>,
    fields: Vec<String>,
)
{
    // Use the provided fields for the command as the ones we'll be showing. We
    // also expand fields, if needed.
    let cells: Vec<CellSpecification> = fields
        .into_iter()
        .map(|f| match &config.query.fields.get(&f)
        {
            None => f,
            Some(qf) => qf.jmes.clone(),
        })
        .map(|f| CellSpecification::new(f.as_str()))
        .collect();

    // Create the tabular data we are going to display to the user.
    let rows = files
        .into_iter()
        .map(|x| {
            let query = &x.query();
            let row_cells = cells.iter().map(|c| c.to_cell(query)).collect();

            Row::new(row_cells)
        })
        .collect();
    let table = Table::init(rows);

    // Write out the table and we're done.
    write_table(table);
}

pub fn write_table(mut table: Table)
{
    // Format the table to look like `ls -l`.
    let format = format::FormatBuilder::new()
        //.column_separator('|')
        //.borders('|')
        //.separators(
        //    &[format::LinePosition::Top, format::LinePosition::Bottom],
        //    format::LineSeparator::new('-', '+', '+', '+'),
        //)
        .padding(0, 1)
        .build();

    // Write out the resulting table.
    table.set_format(format);
    table.printstd();
}

/// Describes a single cell in a dynamic way. This includes formatting and
/// alignment for the cell along with the JMES path used to query the data from
/// inside the project file.
#[derive(Debug)]
pub struct CellSpecification
{
    expr: jmespath::Expression<'static>,
}

impl CellSpecification
{
    pub fn new(spec: &str) -> CellSpecification
    {
        let expr = jmespath::compile(spec).unwrap();

        CellSpecification { expr }
    }

    pub fn to_cell(&self, query: &Value) -> Cell
    {
        match &self.expr.search(query)
        {
            Err(_) => Cell::new(&"".to_string()),
            Ok(r) =>
            {
                if r.is_string()
                {
                    Cell::new(&r.as_string().unwrap().to_owned())
                }
                else if r.is_number()
                {
                    let locale = SystemLocale::default().unwrap();
                    let value = r.as_number().unwrap();
                    let value = value as i32;
                    let value = value.to_formatted_string(&locale);
                    let value = value.as_str();

                    Cell::new_align(value, Alignment::RIGHT)
                }
                else
                {
                    Cell::new(&"".to_string())
                }
            }
        }
    }
}
