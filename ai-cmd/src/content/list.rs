use crate::result::CommandResult;
use crate::table::write_file_list_table;
use crate::CommandContext;
use ai_core::project::*;
use failure;
use log::*;
use std::fmt;

#[derive(Debug)]
pub struct ListContentCommand
{
    pub context: CommandContext,
    pub project: Project,
    pub fields: Vec<String>,
}

impl ListContentCommand
{
    pub fn run(self) -> Result<CommandResult, failure::Error>
    {
        // Scan the files and pull out the information we need.
        debug!("scanning content in project to create list");
        self.project.scan_files()?;

        // Get the content in the project.
        let mut files = self.project.content();

        files.sort();

        // Display the file list.
        write_file_list_table(&self.project.config, files, self.fields);

        Ok(CommandResult::success())
    }
}

impl fmt::Display for ListContentCommand
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        return write!(f, "ListContentCommand {}", self.project);
    }
}
