use crate::result::CommandResult;
use crate::table::write_file_list_table;
use crate::CommandContext;
use ai_core::project::*;
use failure;
use log::*;
use std::fmt;

#[derive(Debug)]
pub struct ListFileCommand
{
    pub context: CommandContext,
    pub project: Project,
    pub fields: Vec<String>,
}

impl ListFileCommand
{
    pub fn run(self) -> Result<CommandResult, failure::Error>
    {
        // Scan the files and pull out the information we need.
        debug!("scanning files in project to create list");
        self.project.scan_files()?;

        // Pull out the files and sort them.
        let mut files = self.project.files();

        files.sort();

        // Display the file list.
        write_file_list_table(&self.project.config, files, self.fields);

        Ok(CommandResult::success())
    }
}

impl fmt::Display for ListFileCommand
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        return write!(f, "ListFileCommand {}", self.project);
    }
}
