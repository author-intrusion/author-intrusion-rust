use crate::{result::CommandResult, CommandContext};
use ai_core::{checker::level::Level, project::*};
use failure::*;
use log::*;
use std::fmt;

#[derive(Debug)]
pub struct CheckCommand
{
    pub context: CommandContext,
    pub project: Project,
    pub report_absolute_paths: bool,
}

impl CheckCommand
{
    pub fn run(self) -> Result<CommandResult, Error>
    {
        // Scan the files and pull out the information we need.
        debug!("scanning files in project to create list");
        self.project.scan_files()?;

        // Gather up all the issues from inside the file.
        info!("building up a list of issues");
        let mut issues = self.project.get_issues();
        info!("found {} issues", &issues.len());
        issues.sort();

        // Loop through the issues and write each one out.
        let iter = issues.into_iter();
        let mut has_errors = false;

        for issue in iter
        {
            // Internally, the indexes and offsets are zero-based but humans
            // line one-based, so we add one here.
            if issue.level == Level::Error
            {
                has_errors = true;
            }

            let path = if self.report_absolute_paths
            {
                issue.loc.abs_path()
            }
            else
            {
                issue.loc.rel_path()
            };

            println!(
                "{}:{}:{}: {}: {}",
                path.to_str().unwrap(),
                issue.span.line + 1,
                issue.span.c.line + 1,
                issue.level.as_str(),
                issue.text,
            );
        }

        // If we have errors, then we return an error.
        match has_errors
        {
            false => Ok(CommandResult::success()),
            true => Ok(CommandResult { exit_code: 1 }),
        }
    }
}

impl fmt::Display for CheckCommand
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        return write!(f, "CheckCommand {}", self.project);
    }
}
