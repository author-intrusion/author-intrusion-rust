use std::path::PathBuf;

pub mod check;
pub mod content;
pub mod file;
pub mod project;
pub mod result;
pub mod table;

/// The base options for most commands used within the system. These options
/// don't have any concept of AI objects, only functionality shared with most
/// Unix-based commands.
#[derive(Debug)]
pub struct CommandContext
{
    /// Contains the current working directory. Typically this is set via the
    /// environment but unit tests may set it to verify logic.
    pub cwd: PathBuf,
}
