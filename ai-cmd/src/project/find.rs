use crate::result::CommandResult;
use crate::CommandContext;
use ai_core::project::*;
use failure;
use std::fmt;

#[derive(Debug)]
pub struct FindProjectCommand
{
    pub context: CommandContext,
    pub project: Project,
}

impl FindProjectCommand
{
    pub fn run(&self) -> Result<CommandResult, failure::Error>
    {
        // We jump through this hoop so we don't print quotes (") around the
        // path of the project.
        println!("{}", self.project.config_path.to_str().unwrap().to_string());

        Ok(CommandResult::success())
    }
}

impl fmt::Display for FindProjectCommand
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(f, "FindProjectCommand {}", self.project)
    }
}
