/// Defines the result coming out of a command.
#[derive(Debug)]
pub struct CommandResult
{
    pub exit_code: i32,
}

impl CommandResult
{
    pub fn success() -> CommandResult
    {
        CommandResult { exit_code: 0 }
    }
}
