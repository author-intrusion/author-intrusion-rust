use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct QueryFieldConfig
{
    pub jmes: String,
}
