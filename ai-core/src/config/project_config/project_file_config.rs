use failure::Error;
use glob::Pattern;
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::path::Path;

#[derive(Debug)]
pub struct ProjectFileConfig
{
    pattern: String,
    rel_pattern: Pattern,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OptionalProjectFileConfig
{
    pattern: String,
}

impl ProjectFileConfig
{
    pub fn new() -> ProjectFileConfig
    {
        ProjectFileConfig {
            pattern: "invalid".to_owned(),
            rel_pattern: Pattern::new("invalid").unwrap(),
        }
    }

    pub fn with_options(
        options: OptionalProjectFileConfig,
    ) -> Result<ProjectFileConfig, Error>
    {
        let rel_pattern = Pattern::new(&options.pattern)?;

        Ok(ProjectFileConfig {
            pattern: options.pattern,
            rel_pattern,
        })
    }

    pub fn is_match(&self, rel_path: &Path) -> bool
    {
        let matches = &self.rel_pattern.matches(&rel_path.to_str().unwrap());

        *matches
    }

    pub fn num(&self, rel_path: &Path) -> Option<u32>
    {
        let rel_path = &rel_path.file_name().unwrap().to_str().unwrap();

        file_num(rel_path)
    }
}

fn file_num(text: &str) -> Option<u32>
{
    lazy_static! {
        static ref RE: Regex = Regex::new("^.*?(\\d+).*?$").unwrap();
    }

    if RE.is_match(text)
    {
        let num = RE.replace(text, "$1");
        let num = num.parse::<u32>().unwrap();

        Some(num)
    }
    else
    {
        None
    }
}
