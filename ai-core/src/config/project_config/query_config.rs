use self::query_field_config::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub mod query_field_config;

#[derive(Debug, Serialize, Deserialize)]
pub struct QueryConfig
{
    pub fields: HashMap<String, QueryFieldConfig>,
}

impl QueryConfig
{
    pub fn new() -> QueryConfig
    {
        QueryConfig {
            fields: HashMap::new(),
        }
    }
}
