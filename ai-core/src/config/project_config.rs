use self::project_file_config::*;
use self::query_config::*;
use crate::checker::*;
use failure::Error;
use log::*;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::PathBuf;

pub mod project_file_config;
pub mod query_config;

#[derive(Debug)]
pub struct ProjectConfig
{
    pub path: PathBuf,

    pub content: ProjectFileConfig,

    pub support: Vec<ProjectFileConfig>,

    pub query: QueryConfig,

    pub checkers: Vec<Checker>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct OptionalProjectConfig
{
    pub content: OptionalProjectFileConfig,

    pub support: Option<Vec<OptionalProjectFileConfig>>,

    pub query: Option<QueryConfig>,

    #[serde(rename = "checks")]
    pub checkers: Option<Vec<OptionalChecker>>,
}

impl ProjectConfig
{
    pub fn new(path: PathBuf) -> ProjectConfig
    {
        ProjectConfig {
            path,
            content: ProjectFileConfig::new(),
            support: Vec::new(),
            query: QueryConfig::new(),
            checkers: Vec::new(),
        }
    }

    pub fn with_options(
        path: PathBuf,
        options: OptionalProjectConfig,
    ) -> Result<ProjectConfig, Error>
    {
        let mut config = ProjectConfig::new(path);
        config.apply_options(options)?;
        Ok(config)
    }

    pub fn apply_options(
        &mut self,
        options: OptionalProjectConfig,
    ) -> Result<(), Error>
    {
        self.content = ProjectFileConfig::with_options(options.content)?;

        if let Some(c) = options.support
        {
            let results: Result<Vec<_>, _> = c
                .into_iter()
                .map(|s| ProjectFileConfig::with_options(s))
                .collect();

            self.support = results?;
        }

        if let Some(c) = options.query
        {
            self.query = c;
        }

        if let Some(c) = options.checkers
        {
            self.checkers = c
                .into_iter()
                .map(|cc| match cc
                {
                    OptionalChecker::Clustered(cv) => Checker::Clustered(
                        clustered::Clustered::with_options(cv),
                    ),
                    OptionalChecker::RequireEpigraph(cv) =>
                    {
                        Checker::RequireEpigraph(
                            require_epigraph::RequireEpigraph::with_options(cv),
                        )
                    }
                })
                .collect();
        }

        Ok(())
    }

    /// Loads a project from the filesystem and returns a wrapped version that
    /// has normalized values.
    pub fn load_file(path: &PathBuf) -> Result<ProjectConfig, Error>
    {
        let yaml = fs::read_to_string(path)?;
        let config = serde_yaml::from_str::<OptionalProjectConfig>(&yaml)?;
        let config = ProjectConfig::with_options(path.clone(), config)?;

        debug!("config {:?}", config);

        Ok(config)
    }
}
