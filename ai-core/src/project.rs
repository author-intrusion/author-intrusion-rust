use crate::checker::issue::Issue;
use crate::config;
use failure::Error;
use file::purpose::Purpose;
use ignore::Walk;
use location::Location;
use log::*;
use pathdiff::diff_paths;
use std::fmt;
use std::path;
use std::path::{Path, PathBuf};
use std::result;
use std::sync::{Arc, RwLock};

pub mod file;
pub mod fs;
pub mod location;

#[derive(Debug)]
pub struct Project
{
    pub config_path: path::PathBuf,
    pub config: config::project_config::ProjectConfig,
    pub root_path: path::PathBuf,
    project_files: Arc<RwLock<Vec<Arc<file::File>>>>,
}

impl Project
{
    /// Constructs a new project and initializes its internal state.
    pub fn new(config_path: PathBuf) -> Result<Project, Error>
    {
        // Figure out the project root path.
        let root_path = &config_path.parent();
        let root_path = match root_path
        {
            Some(p) => p.to_path_buf(),
            _ => panic!("fix me"),
        };

        // Load the configuration into memory.
        let config =
            config::project_config::ProjectConfig::load_file(&config_path)?;

        // Set up the project files access.
        let project_files = Vec::new();
        let project_files = RwLock::new(project_files);
        let project_files = Arc::new(project_files);

        // Construct the project and return it.
        let project = Project {
            config_path,
            config,
            root_path,
            project_files,
        };

        Ok(project)
    }

    /// Registers or inserts a file into the file system. If the file is already
    /// known, this updates the existing file otherwise it adds it.
    pub fn add_file(&self, file: file::File)
    {
        let mut project_files = self.project_files.write().unwrap();
        let file = Arc::new(file);

        project_files.push(file);
    }

    /// Returns a list of files known inside the project.
    pub fn files(&self) -> Vec<Arc<file::File>>
    {
        let project_files;
        {
            project_files = self
                .project_files
                .read()
                .unwrap()
                .iter()
                .map(|x| x.clone())
                .collect();
        }

        project_files
    }

    pub fn content(&self) -> Vec<Arc<file::File>>
    {
        self.files()
            .to_vec()
            .iter()
            .filter(|x| x.purpose == Purpose::Content)
            .map(|x| x.clone())
            .collect()
    }

    /// Walks through the project directory and identifies all the files that
    /// are inside the project.
    pub fn scan_files(&self) -> result::Result<(), Error>
    {
        let dir_path = self.root_path.to_path_buf();

        for result in Walk::new(dir_path)
        {
            match result
            {
                Ok(entry) =>
                {
                    let path = entry.path();

                    if !path.is_file()
                    {
                        continue;
                    }

                    // Figure out some information about the file.
                    let abs_path = path.to_path_buf();
                    let rel_path = get_rel_path(&path, &self.root_path);
                    let (file_type, num) =
                        get_info(&self.config, &abs_path, &rel_path);

                    // Create and push the resulting file.
                    let loc = Location::with_paths(abs_path, rel_path)?;
                    let loc = Arc::new(loc);
                    let project_file = file::File::new(
                        loc,
                        file_type,
                        num,
                        &self.config.checkers,
                    );

                    self.add_file(project_file);
                }
                Err(err) => error!("Cannot parse file: {:?}", err),
            }
        }

        // If we got this far, we're good.
        Ok(())
    }

    pub fn get_issues(&self) -> Vec<Issue>
    {
        let issues: &Vec<Issue> = &self
            .files()
            .iter()
            .flat_map(|f| f.get_issues().unwrap_or(Vec::new()))
            .collect();

        issues.to_owned()
    }
}

impl fmt::Display for Project
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        return write!(f, "Project {}", self.config_path.to_str().unwrap());
    }
}

fn get_rel_path(path: &Path, root_path: &PathBuf) -> PathBuf
{
    let rel_path = diff_paths(path, &root_path);

    match rel_path
    {
        Some(rp) => rp,
        _ => panic!("nope"),
    }
}

fn get_info(
    config: &config::project_config::ProjectConfig,
    abs_path: &Path,
    rel_path: &Path,
) -> (Purpose, Option<u32>)
{
    // If the absolute path matches the project, it is a project type. We don't
    // include other project files by extension though because there may be
    // multiple and we have to be precise when working with files.
    if config.path == abs_path
    {
        return (Purpose::Project, None);
    }

    // See if this matches the content pattern, this is the file pattern that
    // identifies a chapter or scene inside the project.
    let matches = &config.content.is_match(rel_path);

    if *matches
    {
        let num = &config.content.num(rel_path);

        return (Purpose::Content, *num);
    }

    // Loop through the support and see if any match.
    let matches = &config.support.iter().any(|x| x.is_match(rel_path));

    if *matches
    {
        return (Purpose::Support, None);
    }

    // Otherwise, we don't know anything about this file.
    (Purpose::Unknown, None)
}
