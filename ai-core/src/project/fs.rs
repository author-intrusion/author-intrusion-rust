use crate::project::*;
use failure::*;
use std::{
    fs,
    path::{Path, PathBuf},
};

/// Parses the command-line options and pulls out the project-specific
/// elements and encapsulates them into a `Project` object which is
/// used to handle projects.
pub fn get_project(
    working_dir: &Path,
    project_path: Option<String>,
) -> Result<Project, Error>
{
    let project_path = get_project_path(working_dir, project_path)?;

    debug!("using project path {:?}", project_path);

    let project = Project::new(project_path)?;

    Ok(project)
}

fn get_project_path(
    working_dir: &Path,
    path: Option<String>,
) -> Result<PathBuf, Error>
{
    // If the user has provided a project, then we assume they know what
    // that they are doing so we just entire the path is an absolute path
    // (relative to the cwd which may have been provided).
    match path
    {
        None => get_inferred_project_path(working_dir),
        Some(p) => get_provided_project_path(&p),
    }
}

/// Determines the name of the project by looking for a project file in the
/// current working directory and any directory above it.
fn get_inferred_project_path(working_dir: &Path) -> Result<PathBuf, Error>
{
    // Using the current working directory, we need to look for any file that
    // ends with `.aipry`. If there is exactly one of them, then we're good and
    // can use it.
    find_project_file(working_dir, working_dir)
}

/// Looks in the current directory for a project file. If one can't be found,
/// then it moves up and tries again. If more than one is found, then an error
/// is reported and returned.
fn find_project_file(start: &Path, dir: &Path) -> Result<PathBuf, Error>
{
    // Report where we are looking.
    trace!("looking for project file in {:?}", dir);

    // Go through and look for the files that match the `.aipry` pattern. We
    // ignore directories because we don't move down the filesystem.
    let mut paths = Vec::new();

    for entry in fs::read_dir(dir)?
    {
        // Go through the entries and collect the list of projects we have.
        let entry = entry?;
        let path = entry.path();

        if path.is_file()
        {
            match path.extension()
            {
                Some(os_str) => match os_str.to_str()
                {
                    Some("aipry") => paths.push(path),
                    _ => continue,
                },
                _ => continue,
            };
        }
    }

    // If we have zero, then we recuse into the parent. Otherwise, if we have
    // exactly one, then we are going to use it. Finally, if there are more than
    // one, we are going to throw an error.
    match &paths.len()
    {
        0 => match dir.parent()
        {
            None =>
            {
                error!("could not find a project file starting at {:?}", start);
                Err(format_err!("project not found"))
            }
            Some(pdir) => find_project_file(start, pdir),
        },
        1 => Ok(PathBuf::from(&paths[0])),
        _ => panic!("no idea"),
    }
}

fn get_provided_project_path(
    project_path_str: &String,
) -> Result<PathBuf, Error>
{
    // Convert to a path to make it easier to work with.
    let _project_path = Path::new(project_path_str);
    error!("get_provided_project_path not implemented");
    panic!();
}
