use core::cmp::{Ord, Ordering};
use std::path::PathBuf;

#[derive(Clone, Debug, Eq)]
pub struct Location
{
    /// The root directory for the file path. All files inside the project will
    /// have the same root directory.
    abs: String,

    /// The normalized relative path, not starting with "/". This can be
    /// combined with root + rel to come up with the full path.
    rel: String,
}

impl Location
{
    pub fn with_paths(
        abs: PathBuf,
        rel: PathBuf,
    ) -> Result<Location, failure::Error>
    {
        Ok(Location {
            abs: abs.to_string_lossy().to_string(),
            rel: rel.to_string_lossy().to_string(),
        })
    }

    #[cfg(test)]
    pub fn with_blank() -> Location
    {
        Location {
            abs: String::from("/a"),
            rel: String::from("a"),
        }
    }

    pub fn rel_path(&self) -> PathBuf
    {
        PathBuf::from(&self.rel)
    }

    pub fn abs_path(&self) -> PathBuf
    {
        PathBuf::from(&self.abs)
    }
}

impl Ord for Location
{
    fn cmp(&self, other: &Location) -> Ordering
    {
        self.rel.cmp(&other.rel)
    }
}

impl PartialOrd for Location
{
    fn partial_cmp(&self, other: &Location) -> Option<Ordering>
    {
        Some(self.cmp(other))
    }
}

impl PartialEq for Location
{
    fn eq(&self, other: &Location) -> bool
    {
        self.rel == other.rel
    }
}
