use crate::checker::issue::Issue;
use crate::checker::*;
use crate::project::file::metadata::count::*;
use crate::project::file::purpose::Purpose::*;
use crate::project::file::purpose::*;
use crate::project::location::*;
use crate::token::*;
use buffer::ProjectFileBuffer;
use content::{values::ProjectFileContentValues, ProjectFileContent};
use core::cmp::{Ord, Ordering};
use failure::Error;
use issues::ProjectFileIssues;
use serde_json::Value;
use std::fs;
use std::result::Result;
use std::sync::{Arc, RwLock};
use tokens::ProjectFileTokens;

pub mod buffer;
pub mod content;
pub mod issues;
pub mod metadata;
pub mod purpose;
pub mod tokens;

#[derive(Debug, Clone)]
pub struct File
{
    pub location: Arc<Location>,

    pub purpose: Purpose,

    /// The chapter or scene number.
    pub num: Option<u32>,

    /// A handle to the file system contents.
    buffer: Arc<RwLock<ProjectFileBuffer>>,

    /// A handle to the parsed contents of the buffer.
    content: Arc<RwLock<ProjectFileContent>>,

    /// A handle to the parsed tokens of the content.
    tokens: Arc<RwLock<ProjectFileTokens>>,

    /// A handle to the various issues in the file.
    issues: Arc<RwLock<ProjectFileIssues>>,
}

impl File
{
    pub fn new(
        location: Arc<Location>,
        purpose: Purpose,
        num: Option<u32>,
        checkers: &[Checker],
    ) -> File
    {
        let buffer = ProjectFileBuffer::new(location.clone());
        let buffer = RwLock::new(buffer);
        let buffer = Arc::new(buffer);

        let content = ProjectFileContent::new();
        let content = RwLock::new(content);
        let content = Arc::new(content);

        let tokens = ProjectFileTokens::new();
        let tokens = RwLock::new(tokens);
        let tokens = Arc::new(tokens);

        let issues = ProjectFileIssues::new(&checkers);
        let issues = RwLock::new(issues);
        let issues = Arc::new(issues);

        File {
            location,
            purpose,
            num,
            buffer,
            content,
            tokens,
            issues,
        }
    }

    pub fn len(&self) -> std::io::Result<u64>
    {
        let metadata = fs::metadata(&self.location.abs_path())?;
        let size = metadata.len();

        Ok(size)
    }

    pub fn is_empty(&self) -> bool
    {
        match self.len()
        {
            Ok(l) => l == 0,
            Err(_) => false,
        }
    }

    /// Retrieves the contents of the buffer, loading it from the disk as needed
    /// or using the pinned version in memory.
    fn get_buffer(&self) -> Result<String, Error>
    {
        match self.purpose
        {
            Content | Support => ProjectFileBuffer::get_buffer(&self.buffer),
            _ => Ok(String::new()),
        }
    }

    /// Retrieves the split apart content of the buffer into metadata and
    /// content.
    fn get_content(&self) -> Result<ProjectFileContentValues, Error>
    {
        match self.purpose
        {
            Content | Support => ProjectFileContent::get_content(
                &self.location,
                &self.content,
                &|| self.get_buffer(),
            ),
            _ => Ok(ProjectFileContentValues::new()),
        }
    }

    /// Retrieves the tokens that have been parsed out of the contents.
    fn get_tokens(&self) -> Result<Vec<Token>, Error>
    {
        match self.purpose
        {
            Content => ProjectFileTokens::get_tokens(
                &self.location,
                &self.tokens,
                &|| self.get_content(),
            ),
            _ => Ok(vec![]),
        }
    }

    pub fn reset(&self)
    {
        ProjectFileBuffer::reset(&self.buffer);
        ProjectFileContent::reset(&self.content);
        ProjectFileTokens::reset(&self.tokens);
        ProjectFileIssues::reset(&self.issues);
    }

    /// Retrieves the issues associated with this file.
    pub fn get_issues(&self) -> Result<Vec<Issue>, Error>
    {
        match self.purpose
        {
            Content => ProjectFileIssues::get_issues(
                self.location.clone(),
                &self.issues,
                &|| self.get_content(),
                &|| self.get_tokens(),
            ),
            _ => Ok(vec![]),
        }
    }

    /// Creates a metadata object that represents the file and its metadata
    /// (which includes file system and other subsystem components).
    pub fn as_metdata(&self) -> metadata::Metadata
    {
        let purpose = &self.purpose.as_str().to_string();
        let rel_path = &self.location.rel_path();
        let abs_path = &self.location.abs_path();
        let bytes = &self.len().unwrap();
        let num = self.num;
        let tokens = &self.get_tokens();
        let count = match tokens
        {
            Err(_) => None,
            Ok(t) => Some(Count::new(t)),
        };

        metadata::Metadata {
            file: metadata::file::File {
                purpose: purpose.to_owned(),
                bytes: *bytes,
                rel_path: rel_path.to_owned(),
                abs_path: abs_path.to_owned(),
                num,
            },
            count,
        }
    }

    /// Creates a combined query object that merges a file's front matter,
    /// metadata about the file itself (into `_file`), and other calculated or
    /// analyzed data (such as `_count`) into a single variable that can be
    /// queried using JMES.
    pub fn query(&self) -> Value
    {
        // Grab the metadata and use that as the top-level structure. It will
        // already have the components in the `_file` and other calculated
        // fields.
        let metadata = &self.as_metdata();
        let metadata_json = serde_json::to_string(&metadata).unwrap();
        let mut merged: Value =
            serde_json::from_str(&metadata_json.as_str()).unwrap();

        // Because we only load matter for support and content files, the
        // `self.matter` may be `Value::Null` which indicates no data was given
        // or that the file has no metdata. In that case, we just return the
        // merged object. Otherwise, we combine it with the matter and return
        // that.
        let matter = match &self.get_content()
        {
            Ok(r) => r.matter.clone(),
            Err(_) => Value::Null,
        };

        match matter
        {
            Value::Null => merged,
            _ =>
            {
                merge(&mut merged, &matter);
                merged
            }
        }
    }
}

impl Eq for File {}

impl Ord for File
{
    fn cmp(&self, other: &File) -> Ordering
    {
        self.location.cmp(&other.location)
    }
}

impl PartialOrd for File
{
    fn partial_cmp(&self, other: &File) -> Option<Ordering>
    {
        Some(self.location.rel_path().cmp(&other.location.rel_path()))
    }
}

impl PartialEq for File
{
    fn eq(&self, other: &File) -> bool
    {
        self.location == other.location
    }
}

/// Merges two JSON objects together.
///
/// From https://github.com/serde-rs/json/issues/377
fn merge(a: &mut Value, b: &Value)
{
    match (a, b)
    {
        (&mut Value::Object(ref mut a), &Value::Object(ref b)) =>
        {
            for (k, v) in b
            {
                merge(a.entry(k.clone()).or_insert(Value::Null), v);
            }
        }
        (a, b) =>
        {
            *a = b.clone();
        }
    }
}
