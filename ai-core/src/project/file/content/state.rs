/// Describes the current state of a file's parsed content.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum ProjectFileContentState
{
    // Indicates that the content has not been parsed and split into metadata
    // and textual content.
    Unparsed,

    /// Indicates the buffer contents have been parsed.
    Parsed,
}
