use serde_json::value::Value;
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct ProjectFileContentValues
{
    pub text: Arc<String>,
    pub line_index: usize,
    pub matter: Value,
}

impl ProjectFileContentValues
{
    pub fn new() -> ProjectFileContentValues
    {
        ProjectFileContentValues {
            text: Arc::new(String::new()),
            line_index: 0,
            matter: Value::Null,
        }
    }

    pub fn from_str(input: &str) -> ProjectFileContentValues
    {
        ProjectFileContentValues {
            text: Arc::new(String::from(input)),
            line_index: 0,
            matter: Value::Null,
        }
    }
}
