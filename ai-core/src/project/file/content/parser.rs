use serde_json::value::Value;
use serde_json::*;

/// Parses the front matter of a file and extracts the optional matter and
/// returns the line number where the body starts.
pub fn parse(input: &str) -> (Value, String, usize)
{
    // Check the first line, if it isn't a separator, then there is nothing to
    // parse. This includes blank lines. We use a boolean flag to avoid
    // repeating ourself if we find the first separator but don't find the
    // second.
    let mut lines = input.lines();
    let first_line = lines.next();
    let mut has_matter = true;

    if first_line.is_none() || first_line.unwrap() != "---"
    {
        has_matter = false;
    }

    // If the first line is a separator, then look for the next one. If we can't
    // find it, then we set the flag so we can return cleanly.
    let mut separator_index = 0;

    if has_matter
    {
        let separator = lines.enumerate().find(|&x| x.1 == "---");

        if let Some(pair) = separator
        {
            // We add the +1 to handle the initial separator that we popped off
            // before searching.
            separator_index = pair.0 + 1;
        }
        else
        {
            has_matter = false;
        }
    }

    // If we can't find the matter, then just return the entire buffer as the
    // input.
    if !has_matter
    {
        return (json!({}), String::from(input), 0);
    }

    // We found the first and last/next separator so we use that to bisect the
    // input lines into the two components.
    let lines: Vec<_> = input.lines().collect();

    // Parse the matter as YAML.
    let matter = &lines[1..separator_index].join("\n");
    let matter = serde_yaml::from_str(matter);

    if matter.is_err()
    {
        return (json!({}), String::from(input), 0);
    }

    let matter: serde_json::value::Value = matter.unwrap();

    // Combine the body and figure out the line index.
    let body_line = separator_index + 1;
    let body = &lines[body_line..].join("\n");
    let body = body.to_string();

    // Return the results as a tuple.
    ((matter, body, body_line))
}

#[cfg(test)]
mod tests
{
    use crate::project::file::content::parser::parse;

    /// Makes sure that parsing a blank string works correctly.
    #[test]
    fn blank_matter()
    {
        let input = r"";
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 0);
        assert_eq!(body, "");
        assert_eq!(line, 0);
    }

    /// Makes sure that parsing a single line works correctly.
    #[test]
    fn one_line_with_no_matter()
    {
        let input = &["One."].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 0);
        assert_eq!(body, "One.");
        assert_eq!(line, 0);
    }

    /// Makes sure that parsing a single line works correctly.
    #[test]
    fn two_lines_with_no_matter()
    {
        let input = &["One.", "Two."].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 0);
        assert_eq!(body, "One.\nTwo.");
        assert_eq!(line, 0);
    }

    /// Makes sure that parsing with extra blank lines works.
    #[test]
    fn two_lines_with_no_matter_and_blank_lines()
    {
        let input = &["One.", ""].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 0);
        assert_eq!(body, "One.\n");
        assert_eq!(line, 0);
    }

    /// Makes sure that parsing a single line with matter works correctly.
    #[test]
    fn one_line_with_simple_matter()
    {
        let input = &["---", "title: a", "---", "One."].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 1);
        assert_eq!(header["title"], "a");
        assert_eq!(body, "One.");
        assert_eq!(line, 3);
    }

    /// Makes sure that parsing with matter and extra separator.
    #[test]
    fn one_line_with_simple_matter_and_extra_separator()
    {
        let input = &["---", "title: a", "---", "One.", "---"].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 1);
        assert_eq!(header["title"], "a");
        assert_eq!(body, "One.\n---");
        assert_eq!(line, 3);
    }

    /// Make sure an unterminated separator is not parsed as matter.
    #[test]
    fn one_line_with_unterminated_matter()
    {
        let input = &["---", "title: a"].join("\n");
        let (header, body, line) = parse(input);

        assert_eq!(header.as_object().unwrap().len(), 0);
        assert_eq!(body, "---\ntitle: a");
        assert_eq!(line, 0);
    }
}
