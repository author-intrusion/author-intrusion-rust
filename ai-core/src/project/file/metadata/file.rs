use serde_derive::{Deserialize, Serialize};
use std::path::PathBuf;

#[derive(Debug, Serialize, Deserialize)]
pub struct File
{
    /// Contains the lowercase string that represents the file type.
    pub purpose: String,

    /// Contains the length of file in bytes.
    pub bytes: u64,

    /// Contains the relative path inside the project.
    pub rel_path: PathBuf,

    /// Contains the absolute path to the file name.
    pub abs_path: PathBuf,

    /// Contains the optional chapter/scene number.
    pub num: Option<u32>,
}
