use crate::token::Token;
use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Count
{
    pub tokens: usize,
    pub words: usize,
}

impl Count
{
    pub fn new(tokens: &[Token]) -> Count
    {
        let words: Vec<&Token> =
            tokens.iter().filter(|t| t.is_word()).collect();
        let words = words.len();

        Count {
            tokens: tokens.len(),
            words,
        }
    }
}
