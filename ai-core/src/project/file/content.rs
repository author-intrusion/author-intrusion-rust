use crate::project::location::Location;
use failure::Error;
use log::*;
use state::ProjectFileContentState;
use std::sync::{Arc, RwLock};
use values::ProjectFileContentValues;

mod parser;
pub mod state;
pub mod values;

/// Encapsulates the processing of a file's content and metdata.
#[derive(Debug)]
pub struct ProjectFileContent
{
    /// Contains the current state of the buffer.
    state: ProjectFileContentState,

    /// The parsed components, if we have them.
    values: ProjectFileContentValues,
}

impl ProjectFileContent
{
    pub fn new() -> ProjectFileContent
    {
        ProjectFileContent {
            state: ProjectFileContentState::Unparsed,
            values: ProjectFileContentValues::new(),
        }
    }

    /// Retrieves the contents of the buffer, loading it from the disk as needed
    /// or using the pinned version in memory.
    pub fn get_content(
        location: &Location,
        content: &Arc<RwLock<ProjectFileContent>>,
        get: &dyn Fn() -> Result<String, Error>,
    ) -> Result<ProjectFileContentValues, Error>
    {
        // First we check this in read-only mode to allow multiple readers to
        // access it at the same time. This is in a braces so the read-only
        // lock goes out of scope.
        {
            let content = content.read().unwrap();
            let state = &content.state;

            if *state != ProjectFileContentState::Unparsed
            {
                trace!("{:?} returning cached content", &location.rel_path());

                let values = &content.values;

                return Ok(values.clone());
            }
        }

        // We didn't have everything parsed, so we need to get a write lock and
        // try again.
        let mut content = content.write().unwrap();
        let state = &content.state;

        if *state == ProjectFileContentState::Unparsed
        {
            // Get the contents and parse them.
            let buffer = get()?;
            let (matter, text, line_index) = parser::parse(&buffer);
            let text = Arc::new(text);

            trace!("{:?}: header\n{}", &location.rel_path(), &matter);

            content.values = ProjectFileContentValues {
                text,
                line_index,
                matter,
            };

            // No matter what, we're parsed.
            content.state = ProjectFileContentState::Parsed;
        }

        // Retrieve the results and return them.
        debug!("{:?} returning parsed content", &location.rel_path());

        let values = &content.values;

        Ok(values.clone())
    }

    pub fn reset(content: &Arc<RwLock<ProjectFileContent>>)
    {
        let mut content = content.write().unwrap();

        content.state = ProjectFileContentState::Unparsed;
        content.values = ProjectFileContentValues::new();
    }
}
