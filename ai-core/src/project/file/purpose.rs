/// Indicates the various ways a project file is tagged so it can be processed
/// within the code.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum Purpose
{
    /// An unknown file purpose or pattern.
    Unknown,

    /// The project file that is currently being used.
    Project,

    /// A content such as a chapter or scene.
    Content,

    /// A supporting file such as a character or location.
    Support,
}

impl Purpose
{
    pub fn as_str(&self) -> &str
    {
        match self
        {
            Purpose::Unknown => "unknown",
            Purpose::Project => "project",
            Purpose::Content => "content",
            Purpose::Support => "support",
        }
    }
}
