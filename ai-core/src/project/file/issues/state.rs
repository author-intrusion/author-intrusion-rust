/// Describes the current state of a file's parsed content as tokens.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum ProjectFileIssuesState
{
    // Indicates that issues have not been identified.
    Unchecked,

    /// Indicates the contents have been parsed and issues identified.
    Checked,
}
