use crate::project::location::Location;
use failure::Error;
use log::*;
use state::ProjectFileBufferState;
use std::fs;
use std::sync::{Arc, RwLock};

pub mod state;

/// Encapsulates the file handle and buffer information about a project file.
/// This is used as an atomic element to handle internal mutability of the
/// project when various processes need an up-to-date version of the buffer or
/// the buffer is being modified through a LSP server.
#[derive(Debug)]
pub struct ProjectFileBuffer
{
    /// The location of the project file.
    pub location: Arc<Location>,

    /// Contains the current usage mode for the file buffer.
    pub state: ProjectFileBufferState,

    /// The optional content of the buffer. If the mode is Unloaded, this will
    /// be `None`. Otherwise it will be `Some(string)`.
    pub content: String,
}

impl ProjectFileBuffer
{
    pub fn new(location: Arc<Location>) -> ProjectFileBuffer
    {
        ProjectFileBuffer {
            location,
            state: ProjectFileBufferState::Unloaded,
            content: String::new(),
        }
    }

    /// Retrieves the contents of the buffer, loading it from the disk as needed
    /// or using the pinned version in memory.
    pub fn get_buffer(
        buffer: &Arc<RwLock<ProjectFileBuffer>>,
    ) -> Result<String, Error>
    {
        // If the mode indicates we're loaded, then we return the value. We
        // first do this in a read-only approach to avoid locking the entire
        // system. If the file is unloaded, then we have to lock and get it
        // before returning the results.
        {
            let buffer = buffer.read().unwrap();
            let state = &buffer.state;

            if *state != ProjectFileBufferState::Unloaded
            {
                trace!(
                    "{:?} returning cached buffer",
                    &buffer.location.rel_path()
                );

                return Ok(buffer.content.to_string());
            }
        }

        // We handled the "easy" case of the file already being loaded but it
        // wasn't. So this time, we need to get a write lock (which blocks
        // everything) and then try again. We do have to do the same checkers
        // though because another thread may have updated between these two
        // blocks.
        let mut buffer = buffer.write().unwrap();

        match &buffer.state
        {
            ProjectFileBufferState::Unloaded =>
            {
                let content = fs::read_to_string(&buffer.location.abs_path())?;

                debug!(
                    "{:?} loading buffer from fs",
                    &buffer.location.rel_path()
                );

                buffer.content = content.to_string();
                buffer.state = ProjectFileBufferState::Loaded;

                Ok(content)
            }
            _ =>
            {
                trace!(
                    "{:?} returning already loaded buffer",
                    &buffer.location.rel_path()
                );

                let content = &buffer.content;

                Ok(content.to_string())
            }
        }
    }

    pub fn reset(buffer: &Arc<RwLock<ProjectFileBuffer>>)
    {
        let mut buffer = buffer.write().unwrap();

        if buffer.state != ProjectFileBufferState::Locked
        {
            buffer.state = ProjectFileBufferState::Unloaded;
            buffer.content = String::new();
        }
    }
}
