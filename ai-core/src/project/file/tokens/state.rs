/// Describes the current state of a file's parsed content as tokens.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum ProjectFileTokensState
{
    // Indicates that the content has not been tokenized.
    Unparsed,

    /// Indicates the buffer contents have been tokenized.
    Parsed,
}
