use serde::{Deserialize, Serialize};

pub mod count;
pub mod file;

/// Implements a view into the project file that contains the various metadata
/// components that can be queried or calculated.
#[derive(Debug, Serialize, Deserialize)]
pub struct Metadata
{
    pub file: file::File,
    pub count: Option<count::Count>,
}
