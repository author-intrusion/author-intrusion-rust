/// Describes the mode of how to update or manage the buffer for the file. This
/// controls if the buffer should automatically load from the disk on changes
/// or if it is being "locked" by the LSP which will be sending changes via that
/// protocol.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum ProjectFileBufferState
{
    /// Indicates that the buffer contents have not been loaded and they need
    /// to be before the buffer can be used.
    Unloaded,

    /// Indicates that the buffer contents have been loaded and are ready to
    /// be used by the system.
    Loaded,

    /// Indicates that the buffer is pinned or locked by a process. It assumes
    /// the contents are loaded or, more importantly, what they should be
    /// processed as.
    Locked,
}
