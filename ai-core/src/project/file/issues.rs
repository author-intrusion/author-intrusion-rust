use crate::checker::issue::Issue;
use crate::checker::Checker;
use crate::checker::Checker::Clustered;
use crate::checker::Checker::RequireEpigraph;
use crate::project::file::content::values::ProjectFileContentValues;
use crate::project::location::Location;
use crate::token::*;
use failure::Error;
use log::*;
use state::ProjectFileIssuesState;
use std::sync::{Arc, RwLock};

pub mod state;

/// Encapsulates the processing of a file's content and metdata.
#[derive(Debug)]
pub struct ProjectFileIssues
{
    checkers: Vec<Checker>,

    /// Contains the current state of the buffer.
    state: ProjectFileIssuesState,

    issues: Vec<Issue>,
}

impl ProjectFileIssues
{
    pub fn new(checkers: &[Checker]) -> ProjectFileIssues
    {
        let checkers = checkers.to_vec();

        ProjectFileIssues {
            checkers,
            state: ProjectFileIssuesState::Unchecked,
            issues: Vec::new(),
        }
    }

    pub fn get_issues(
        location: Arc<Location>,
        issues: &Arc<RwLock<ProjectFileIssues>>,
        get_content: &dyn Fn() -> Result<ProjectFileContentValues, Error>,
        get_tokens: &dyn Fn() -> Result<Vec<Token>, Error>,
    ) -> Result<Vec<Issue>, Error>
    {
        // First we check this in read-only mode to allow multiple readers to
        // access it at the same time. This is in a braces so the read-only
        // lock goes out of scope.
        {
            let issues = issues.read().unwrap();
            let state = &issues.state;

            if *state != ProjectFileIssuesState::Unchecked
            {
                let issues = &issues.issues;
                let issues: Vec<Issue> =
                    issues.iter().map(|x| x.clone()).collect();

                trace!(
                    "{:?} returning {} cached issues",
                    location.rel_path(),
                    issues.len()
                );

                return Ok(issues);
            }
        }

        // We didn't have everything parsed, so we need to get a write lock and
        // try again.
        let mut issues = issues.write().unwrap();
        let state = &issues.state;

        if *state == ProjectFileIssuesState::Unchecked
        {
            // Check for issues.
            let checkers = &issues.checkers;
            let tokens = get_tokens()?;
            let content = get_content()?;
            let found: &Vec<Issue> = &checkers
                .iter()
                .flat_map(|c| match c
                {
                    Clustered(op) => op.check(location.clone(), &tokens),
                    RequireEpigraph(op) => op.check(location.clone(), &content),
                })
                .collect();

            debug!("found {} issues", &found.len());

            // No matter what, we're parsed.
            issues.issues = found.to_vec();
            issues.state = ProjectFileIssuesState::Checked;
        }

        // Retrieve the results and return them.
        trace!("{:?} returning new issues", location.rel_path());

        let issues = &issues.issues;
        let issues = issues.iter().map(|x| x.clone()).collect();

        Ok(issues)
    }

    pub fn reset(issues: &Arc<RwLock<ProjectFileIssues>>)
    {
        let mut issues = issues.write().unwrap();

        issues.state = ProjectFileIssuesState::Unchecked;
        issues.issues = Vec::new();
    }
}
