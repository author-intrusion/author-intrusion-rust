use crate::project::file::content::values::ProjectFileContentValues;
use crate::project::location::Location;
use crate::token::builder::*;
use crate::token::*;
use failure::Error;
use log::*;
use state::ProjectFileTokensState;
use std::sync::{Arc, RwLock};

pub mod state;

/// Encapsulates the processing of a file's content and metdata.
#[derive(Debug)]
pub struct ProjectFileTokens
{
    /// Contains the current state of the buffer.
    state: ProjectFileTokensState,

    tokens: Vec<Token>,
}

impl ProjectFileTokens
{
    pub fn new() -> ProjectFileTokens
    {
        ProjectFileTokens {
            state: ProjectFileTokensState::Unparsed,
            tokens: vec![],
        }
    }

    /// Retrieves the contents of the buffer, loading it from the disk as needed
    /// or using the pinned version in memory.
    pub fn get_tokens(
        location: &Location,
        tokens: &Arc<RwLock<ProjectFileTokens>>,
        get: &dyn Fn() -> Result<ProjectFileContentValues, Error>,
    ) -> Result<Vec<Token>, Error>
    {
        // First we check this in read-only mode to allow multiple readers to
        // access it at the same time. This is in a braces so the read-only
        // lock goes out of scope.
        {
            let tokens = tokens.read().unwrap();
            let state = &tokens.state;

            if *state != ProjectFileTokensState::Unparsed
            {
                trace!("{:?} returning loaded tokens", location.rel_path());

                let tokens = &tokens.tokens;
                let tokens = tokens.iter().map(|x| x.clone()).collect();

                return Ok(tokens);
            }
        }

        // We didn't have everything parsed, so we need to get a write lock and
        // try again.
        let mut tokens = tokens.write().unwrap();
        let state = &tokens.state;

        if *state == ProjectFileTokensState::Unparsed
        {
            // Get the contents and parse them.
            let values = get()?;
            let buffer = values.text.clone();

            tokens.tokens = Builder::build(buffer, values.line_index);
            tokens.state = ProjectFileTokensState::Parsed;
        }

        // Retrieve the results and return them.
        trace!("{:?} returning now loaded tokens", location.rel_path());

        let tokens = &tokens.tokens;
        let tokens = tokens.iter().map(|x| x.clone()).collect();

        Ok(tokens)
    }

    pub fn reset(tokens: &Arc<RwLock<ProjectFileTokens>>)
    {
        let mut tokens = tokens.write().unwrap();

        tokens.state = ProjectFileTokensState::Unparsed;
        tokens.tokens = Vec::new();
    }
}
