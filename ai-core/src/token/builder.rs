use crate::token::span::Span;
use crate::token::*;
use log::*;
use position::Position;
use std::sync::Arc;

mod position;

#[derive(Debug)]
pub struct Builder
{
    /// Contains the buffer used for breaking apart the tokens.
    buffer: Arc<String>,

    /// A list of all the tokens we've generated.
    buffer_tokens: Vec<Token>,

    /// The zero-based current line inside the buffer.
    line_index: usize,

    /// The location where the line starts.
    line_pos: Position,

    /// The location where the current token starts.
    token_pos: Option<Position>,

    last_was_carriage_return: bool,
    last_was_punct: bool,

    /// The location of the quote, if we have one started.
    quote_pos: Option<Position>,
}

impl Builder
{
    pub fn build(buffer: Arc<String>, line_offset: usize) -> Vec<Token>
    {
        let builder = Builder {
            buffer: buffer.clone(),
            buffer_tokens: Vec::new(),
            line_index: line_offset,
            line_pos: Position::init(),
            token_pos: None,
            quote_pos: None,
            last_was_carriage_return: false,
            last_was_punct: false,
        };

        builder.build_vector()
    }

    pub fn build_vector(mut self) -> Vec<Token>
    {
        // We figure out the various components for the buffer in a single pass.
        // We don't care about byte-index into the file, so we can use a simple
        // character loop to figure this out. The `char_indices` uses byte
        // indexes into the buffer but tokens are based on character indices to
        // handle UTF-8.
        let mut char_index = 0;
        let mut char_first = true;
        let buffer = self.buffer.clone();

        for (pos, c) in buffer.char_indices()
        {
            // Increment the character index, but only the first because we have
            // a usize which means we can't start at -1.
            if char_first
            {
                char_first = false;
            }
            else
            {
                char_index += 1;
            }

            let pos = Position::new(pos, char_index);

            trace!("processing {:?} - {}", pos, c);

            // Figure out what to do based on the next lines.
            if self.do_newlines(pos, c)
            {
                trace!("newline");
                continue;
            }

            if self.do_whitespace(pos, c)
            {
                trace!("whitespace");
                continue;
            }

            if c.is_alphanumeric()
            {
                trace!("alphanumeric");
                self.do_char(pos);
                continue;
            }

            &self.do_punct(pos, c);
            trace!("punct");
        }

        // Finish up the last token.
        let pos = Position::new(self.buffer.len(), self.buffer.chars().count());
        self.end_token(pos);

        // Finish up with the list of tokens we've built.
        self.buffer_tokens
    }

    fn do_newlines(&mut self, pos: Position, c: char) -> bool
    {
        // Since we need to translate tokens into (line, column) for
        // reporting, we handle the newline processing first. Once we have
        // those, we can ignore those for the rest of the calls.
        match c
        {
            '\n' =>
            {
                if self.last_was_carriage_return
                {
                    self.line_pos = pos.shift(1);
                    true
                }
                else
                {
                    self.end_token(pos);
                    self.line_index += 1;
                    self.line_pos = pos.shift(1);
                    true
                }
            }
            '\r' =>
            {
                self.end_token(pos);
                self.line_index += 1;
                self.line_pos = pos.shift(1);
                self.last_was_carriage_return = true;
                true
            }
            _ =>
            {
                self.last_was_carriage_return = false;
                false
            }
        }
    }

    fn do_whitespace(&mut self, pos: Position, c: char) -> bool
    {
        if c.is_whitespace()
        {
            self.end_token(pos);
            true
        }
        else
        {
            false
        }
    }

    fn do_char(&mut self, pos: Position)
    {
        // If the last was punctuation, then finish it.
        if self.last_was_punct
        {
            self.end_token(pos);
            self.last_was_punct = false;
        }

        // If we had a single quote last, then merge it into this one.
        self.quote_pos = None;

        // Figure out if we are creating a new token or adding to an existing.
        if self.token_pos.is_none()
        {
            self.token_pos = Some(pos);
        }
    }

    fn do_punct(&mut self, pos: Position, c: char)
    {
        // If we have a single quote, handle the (possible) contraction.
        if c == '\'' && self.token_pos.is_some()
        {
            self.quote_pos = Some(pos);
            return;
        }

        // Punctuation is always its own token. So if we have a token already,
        // then write it out.
        if self.token_pos.is_some()
        {
            self.end_token(pos);
        }

        // Start up a new token. We set the `last_was_punct` so we know to
        // end this token if we have a character next.
        self.do_char(pos);
        self.last_was_punct = true;
    }

    fn end_token(&mut self, pos: Position)
    {
        // See if we have a single quote. If we do, then we have a token
        // before the single quote which we have to add, and then add the
        // single quote into the list.
        let line_pos = self.line_pos;

        if let Some(quote) = self.quote_pos
        {
            let token_pos = &self.token_pos.unwrap();

            trace!(
                "end_token:quote {:?} -  {:?} -  {:?}",
                line_pos,
                token_pos,
                quote
            );

            self.add_token(&token_pos, &quote);
            self.add_token(&quote, &pos);
            self.quote_pos = None;
            self.token_pos = None;
        }

        // If we have a start then we have a token to add. This will
        // be blank when we are calling this at the beginning and end of the
        // buffer.
        if let Some(token_pos) = self.token_pos
        {
            trace!("end_token:note {:?} -  {:?}", &token_pos, &pos);

            self.add_token(&token_pos, &pos);
            self.token_pos = None;
            self.quote_pos = None;
        }
    }

    fn add_token(&mut self, start: &Position, stop: &Position)
    {
        trace!("add_token start {:?} stop {:?}", start, stop);

        let loc = Span::new(
            self.line_index,
            Position::byte_span_unit(&self.line_pos, start, stop),
            Position::char_span_unit(&self.line_pos, start, stop),
        );
        let token = Token::new(self.buffer.clone(), loc);

        &self.buffer_tokens.push(token);
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn blank_string()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(0, tokens.len());
    }

    #[test]
    fn newline()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("\n");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(0, tokens.len());
    }

    #[test]
    fn single_character_string()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(1, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
    }

    #[test]
    fn two_character_strings()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a b");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(2, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("b", true), text_word(&tokens[1]));
        assert_eq!((1, 2, 0, 2), span(&tokens[1]));
    }

    #[test]
    fn two_character_strings_with_newline()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a\nb");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(2, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("b", true), text_word(&tokens[1]));
        assert_eq!((1, 2, 1, 0), span(&tokens[1]));
    }

    #[test]
    fn four_character_strings_with_newline()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a b\nc d");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(4, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("b", true), text_word(&tokens[1]));
        assert_eq!((1, 2, 0, 2), span(&tokens[1]));
        assert_eq!(("c", true), text_word(&tokens[2]));
        assert_eq!((1, 4, 1, 0), span(&tokens[2]));
        assert_eq!(("d", true), text_word(&tokens[3]));
        assert_eq!((1, 6, 1, 2), span(&tokens[3]));
    }

    #[test]
    fn four_character_strings_with_newline_and_offset()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a b\nc d");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 5);

        assert_eq!(4, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((1, 0, 5, 0), span(&tokens[0]));
        assert_eq!(("b", true), text_word(&tokens[1]));
        assert_eq!((1, 2, 5, 2), span(&tokens[1]));
        assert_eq!(("c", true), text_word(&tokens[2]));
        assert_eq!((1, 4, 6, 0), span(&tokens[2]));
        assert_eq!(("d", true), text_word(&tokens[3]));
        assert_eq!((1, 6, 6, 2), span(&tokens[3]));
    }

    #[test]
    fn two_character_strings_with_punct()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("a. b.");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(4, tokens.len());
        assert_eq!(("a", true), text_word(&tokens[0]));
        assert_eq!((".", false), text_word(&tokens[1]));
        assert_eq!(("b", true), text_word(&tokens[2]));
        assert_eq!((".", false), text_word(&tokens[3]));
    }

    #[test]
    fn contraction()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("can't");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(1, tokens.len());
        assert_eq!("can't", tokens[0].text());
        assert_eq!(("can't", true), text_word(&tokens[0]));
    }

    #[test]
    fn trailing_posessive()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("Luis' ball.");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(4, tokens.len());
        assert_eq!("Luis", tokens[0].text());
        assert_eq!("'", tokens[1].text());
        assert_eq!("ball", tokens[2].text());
        assert_eq!(".", tokens[3].text());
    }

    #[test]
    fn quoted_contraction()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("'can't'");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        debug!("tokens {:?}", tokens);

        assert_eq!(3, tokens.len());
        assert_eq!(("'", false), text_word(&tokens[0]));
        assert_eq!(("can't", true), text_word(&tokens[1]));
        assert_eq!(("'", false), text_word(&tokens[2]));
    }

    #[test]
    fn quoted()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("'a'");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(3, tokens.len());
        assert_eq!(("'", false), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("a", true), text_word(&tokens[1]));
        assert_eq!((1, 1, 0, 1), span(&tokens[1]));
        assert_eq!(("'", false), text_word(&tokens[2]));
        assert_eq!((1, 2, 0, 2), span(&tokens[2]));
    }

    #[test]
    fn single_quote()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("'");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(1, tokens.len());
        assert_eq!(("'", false), text_word(&tokens[0]));
        assert_eq!((1, 0, 0, 0), span(&tokens[0]));
    }

    #[test]
    fn two_short_strings()
    {
        test_logger::ensure_env_logger_initialized();

        let input = String::from("aa bb");
        let input = Arc::new(input);
        let tokens = Builder::build(input, 0);

        assert_eq!(2, tokens.len());
        assert_eq!(("aa", true), text_word(&tokens[0]));
        assert_eq!((2, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("bb", true), text_word(&tokens[1]));
        assert_eq!((2, 3, 0, 3), span(&tokens[1]));
    }

    #[test]
    fn two_short_unicode_strings()
    {
        test_logger::ensure_env_logger_initialized();

        let input = Arc::new(String::from("àè ìò"));
        let tokens = Builder::build(input, 0);

        debug!("tokens {:?}", tokens);

        assert_eq!(2, tokens.len());
        assert_eq!(&2, &tokens[0].span.c.len);
        assert_eq!(&4, &tokens[0].span.b.len);
        assert_eq!(("àè", true), text_word(&tokens[0]));
        assert_eq!((2, 0, 0, 0), span(&tokens[0]));
        assert_eq!(("ìò", true), text_word(&tokens[1]));
        assert_eq!((2, 3, 0, 3), span(&tokens[1]));
    }

    fn text_word(token: &Token) -> (&str, bool)
    {
        (token.text(), token.is_word())
    }

    fn span(token: &Token) -> (usize, usize, usize, usize)
    {
        let span = token.span;

        (span.c.len, span.c.buf, span.line, span.c.line)
    }
}
