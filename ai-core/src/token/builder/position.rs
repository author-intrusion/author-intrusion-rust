use crate::token::span::span_unit::SpanUnit;

#[derive(Debug, Clone, Copy)]
pub struct Position
{
    pub byte: usize,
    pub char: usize,
}

impl Position
{
    pub fn new(byte: usize, char: usize) -> Position
    {
        Position { byte, char }
    }

    pub fn init() -> Position
    {
        Position::new(0, 0)
    }

    pub fn shift(&self, delta: usize) -> Position
    {
        Position::new(&self.byte + delta, self.char + delta)
    }

    pub fn byte_span_unit(
        line: &Position,
        start: &Position,
        stop: &Position,
    ) -> SpanUnit
    {
        SpanUnit::new(start.byte, stop.byte - line.byte, stop.byte - start.byte)
    }

    pub fn char_span_unit(
        line: &Position,
        start: &Position,
        stop: &Position,
    ) -> SpanUnit
    {
        SpanUnit::new(
            start.char,
            start.char - line.char,
            stop.char - start.char,
        )
    }
}
