use core::cmp::{Ord, Ordering};
use span_unit::SpanUnit;
use std::ops::Range;

pub mod span_unit;

/// Represents a span of text within a text buffer.
#[derive(Copy, Clone, Debug)]
pub struct Span
{
    // The line inside the buffer that represents this span.
    pub line: usize,

    // The information about the span in terms of utf8-bytes.
    pub b: SpanUnit,

    // The information about the span in terms of human-readable characters.
    pub c: SpanUnit,
}

impl Span
{
    pub fn new(line: usize, b: SpanUnit, c: SpanUnit) -> Span
    {
        Span { line, b, c }
    }

    pub fn with_ascii(
        line: usize,
        buf_offset: usize,
        line_offset: usize,
        len: usize,
    ) -> Span
    {
        let span_unit = SpanUnit::new(buf_offset, line_offset, len);

        Span {
            line,
            b: span_unit,
            c: span_unit,
        }
    }

    #[cfg(test)]
    pub fn with_test() -> Span
    {
        Span {
            line: 0,
            b: SpanUnit::with_test(),
            c: SpanUnit::with_test(),
        }
    }

    /// Returns this span as a range.
    pub fn as_utf8_range(&self) -> Range<usize>
    {
        Range {
            start: self.b.buf,
            end: self.b.buf + self.b.len,
        }
    }

    /// Returns true if this span represents a zero-length item.
    pub fn is_empty(&self) -> bool
    {
        self.b.len == 0
    }
}

impl Eq for Span {}

impl Ord for Span
{
    fn cmp(&self, other: &Span) -> Ordering
    {
        self.b.buf.cmp(&other.b.buf)
    }
}

impl PartialOrd for Span
{
    fn partial_cmp(&self, other: &Span) -> Option<Ordering>
    {
        Some(self.b.buf.cmp(&other.b.buf))
    }
}

impl PartialEq for Span
{
    fn eq(&self, other: &Span) -> bool
    {
        self.b.buf == other.b.buf
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn single_character_span()
    {
        let span_unit = SpanUnit::new(0, 0, 1);
        let span = Span::new(0, span_unit, span_unit);

        assert_eq!(1, span.b.len);
        assert_eq!(false, span.is_empty());
    }

    #[test]
    fn zero_character_span()
    {
        let span_unit = SpanUnit::new(0, 0, 0);
        let span = Span::new(0, span_unit, span_unit);

        assert_eq!(0, span.b.len);
        assert_eq!(true, span.is_empty());
    }
}
