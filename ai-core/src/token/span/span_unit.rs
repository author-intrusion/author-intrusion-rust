/// Represents the span information for a specific unit.
#[derive(Copy, Clone, Debug)]
pub struct SpanUnit
{
    /// The offset into the buffer with 0 being the starting position.
    pub buf: usize,

    // The offset into the current line.
    pub line: usize,

    // The length of the token.
    pub len: usize,
}

impl SpanUnit
{
    pub fn new(buf: usize, line: usize, len: usize) -> SpanUnit
    {
        SpanUnit { buf, line, len }
    }

    #[cfg(test)]
    pub fn with_test() -> SpanUnit
    {
        SpanUnit {
            buf: 0,
            line: 0,
            len: 0,
        }
    }
}
