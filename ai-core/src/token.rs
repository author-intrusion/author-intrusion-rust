use crate::token::span::Span;
use std::sync::Arc;

pub mod builder;
pub mod span;

/// Defines an immutable token representing a distinct block of text within the
/// given buffer. A token usually identifies a word or puncutation.
#[derive(Debug, Clone)]
pub struct Token
{
    /// Contains a thread-safe reference to the underlying buffer that had been
    /// tokenized.
    pub buffer: Arc<String>,

    /// Contains the position within the buffer that represents this token.
    pub span: Span,
}

impl Token
{
    /// Creates a new buffer based on the given buffer and span.
    pub fn new(buffer: Arc<String>, span: Span) -> Token
    {
        Token { buffer, span }
    }

    /// Retrieves the text value of the token.
    pub fn text(&self) -> &str
    {
        &self.buffer[self.span.as_utf8_range()]
    }

    /// Retrieves the case-insensitive (lowercase) version of the text.
    pub fn itext(&self) -> String
    {
        String::from(self.text()).to_lowercase()
    }

    pub fn is_word(&self) -> bool
    {
        // We can safely use unwrap because we have a length check first.
        !self.span.is_empty()
            && self.text().chars().nth(0).unwrap().is_alphanumeric()
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    use crate::token::span::span_unit::SpanUnit;

    #[test]
    fn single_letter()
    {
        let buffer = String::from("A");
        let buffer = Arc::new(buffer);
        let span_unit = SpanUnit::new(0, 0, 1);
        let span = Span::new(0, span_unit, span_unit);
        let token = Token::new(buffer, span);

        assert_eq!("A", token.text());
        assert_eq!("a", token.itext());
        assert_eq!(true, token.is_word());
    }

    #[test]
    fn empty_span()
    {
        let buffer = String::from("A");
        let buffer = Arc::new(buffer);
        let span_unit = SpanUnit::new(0, 0, 0);
        let span = Span::new(0, span_unit, span_unit);
        let token = Token::new(buffer, span);

        assert_eq!("", token.text());
        assert_eq!("", token.itext());
        assert_eq!(false, token.is_word());
    }

    #[test]
    fn period()
    {
        let buffer = String::from(".");
        let buffer = Arc::new(buffer);
        let span_unit = SpanUnit::new(0, 0, 1);
        let span = Span::new(0, span_unit, span_unit);
        let token = Token::new(buffer, span);

        assert_eq!(".", token.text());
        assert_eq!(".", token.itext());
        assert_eq!(false, token.is_word());
    }
}
