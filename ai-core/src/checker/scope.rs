use crate::token::Token;
use serde::{Deserialize, Serialize};
use std::iter::Iterator;

/// Indicates the scope of processing in a file.
#[derive(
    Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize,
)]
pub enum Scope
{
    /// Indicates that all the tokens in a file should be considered as a single
    /// list. This is the default operation but also the most expensive.
    #[serde(alias = "file")]
    File,
}

impl Scope
{
    pub fn to_iter(&self, tokens: &Vec<Token>) -> ScopeIter
    {
        let tokens: Vec<Token> =
            tokens.into_iter().map(|t| t.clone()).collect();

        ScopeIter { tokens, index: 0 }
    }
}

pub struct ScopeIter
{
    tokens: Vec<Token>,
    index: usize,
}

impl Iterator for ScopeIter
{
    type Item = Vec<Token>;

    fn next(&mut self) -> Option<Vec<Token>>
    {
        // If we've gone past the index, then we're done.
        if &self.index >= &self.tokens.len()
        {
            return None;
        }

        // Move to the next item.
        let tokens = &self.tokens;
        let tokens: Vec<Token> =
            tokens.into_iter().map(|t| t.clone()).collect();

        self.index = std::usize::MAX;
        Some(tokens)
    }
}

#[cfg(test)]
mod tests
{
    use super::*;
    use crate::token::span::Span;
    use std::sync::Arc;

    #[test]
    fn select_file()
    {
        let input = String::from("t1 t2");
        let input = Arc::new(input);
        let tokens = vec![
            Token::new(input.clone(), Span::with_ascii(0, 0, 0, 2)),
            Token::new(input.clone(), Span::with_ascii(3, 3, 3, 2)),
        ];
        let scope = Scope::File;
        let mut iter = scope.to_iter(&tokens);

        let iter_tokens = iter.next();
        assert_eq!(true, iter_tokens.is_some());

        let iter_tokens = iter_tokens.unwrap();
        assert_eq!(2, iter_tokens.len());
        assert_eq!("t1", iter_tokens[0].text());
        assert_eq!("t2", iter_tokens[1].text());

        let iter_tokens = iter.next();
        assert_eq!(true, iter_tokens.is_none());
    }
}
