use self::compare_operation::filter_list::*;
use self::compare_operation::filter_non_words::*;
use self::compare_operation::lowercase::*;
use self::compare_operation::replace_regex::*;
use crate::checker::compare::compare_operation::*;
use crate::checker::compare::compare_select::*;
use crate::checker::compare::compare_split::*;
use crate::checker::compare::compare_value::*;
use crate::token::Token;
use serde::{Deserialize, Serialize};

pub mod compare_operation;
pub mod compare_select;
pub mod compare_split;
pub mod compare_value;

/// Defines the common configuration elements used when comparing tokens to each
/// other, such as for the clusters checker.
#[derive(Debug, Clone)]
pub struct Compare
{
    split: CompareSplit,
    select: CompareSelect,
    operations: Vec<CompareOperation>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct OptionalCompare
{
    pub split: Option<CompareSplit>,
    pub select: Option<CompareSelect>,
    pub operations: Option<Vec<OptionalCompareOperation>>,
}

impl Compare
{
    pub fn new() -> Compare
    {
        Compare {
            split: CompareSplit::Token,
            select: CompareSelect::Lowercase,
            operations: Vec::new(),
        }
    }

    pub fn apply_options(&mut self, options: OptionalCompare)
    {
        if let Some(v) = options.split
        {
            self.split = v;
        }

        if let Some(v) = options.select
        {
            self.select = v;
        }

        if let Some(operations) = options.operations
        {
            self.operations = operations
                .into_iter()
                .map(|op| match op
                {
                    OptionalCompareOperation::FilterList(o) =>
                    {
                        CompareOperation::FilterList(FilterList::with_options(
                            o,
                        ))
                    }
                    OptionalCompareOperation::FilterNonWords(o) =>
                    {
                        CompareOperation::FilterNonWords(
                            FilterNonWords::with_options(o),
                        )
                    }
                    OptionalCompareOperation::Lowercase(o) =>
                    {
                        CompareOperation::Lowercase(Lowercase::with_options(o))
                    }
                    OptionalCompareOperation::ReplaceRegex(o) =>
                    {
                        CompareOperation::ReplaceRegex(
                            ReplaceRegex::with_options(o),
                        )
                    }
                })
                .collect();
        }
    }

    pub fn values(&self, tokens: &Vec<Token>) -> Vec<CompareValue>
    {
        // If we have an empty list, then we have an empty results.
        if &tokens.len() == &0
        {
            return Vec::new();
        }

        // Create an iterator that will split the tokens into the components we
        // are comparing.
        let mut values: Vec<CompareValue> = Vec::new();

        for slice in self.split.to_iter(tokens)
        {
            let value = &self.select.value(&slice);
            let value = value.to_string();
            let mut value = CompareValue {
                value,
                tokens: slice,
                skip_processing: false,
            };
            let mut is_filtered = false;

            for operation in &self.operations
            {
                match operation
                {
                    CompareOperation::FilterList(op) =>
                    {
                        if op.filter(&mut value)
                        {
                            is_filtered = true;
                            break;
                        }
                    }
                    CompareOperation::FilterNonWords(op) =>
                    {
                        if op.filter(&mut value)
                        {
                            is_filtered = true;
                            break;
                        }
                    }
                    CompareOperation::Lowercase(op) => op.update(&mut value),
                    CompareOperation::ReplaceRegex(op) => op.update(&mut value),
                };
            }

            // If an operation says this value is filtered, we skip it.
            if !is_filtered
            {
                values.push(value);
            }
        }

        // Return the gathered list of values.
        values
    }
}

#[cfg(test)]
mod tests
{
    use crate::checker::compare::compare_operation::replace_regex::*;
    use crate::checker::compare::compare_operation::*;
    use crate::checker::compare::compare_select::CompareSelect;
    use crate::checker::compare::compare_split::CompareSplit;
    use crate::checker::compare::compare_value::CompareValue;
    use crate::checker::compare::Compare;
    use crate::token::span::Span;
    use crate::token::Token;
    use std::sync::Arc;

    #[test]
    fn split_token_select_text()
    {
        // Create the list of tokens, assuming it is already scoped properly.
        let input = Arc::new(String::from("t1 t2"));
        let tokens = vec![
            Token::new(input.clone(), Span::with_ascii(0, 0, 0, 2)),
            Token::new(input.clone(), Span::with_ascii(3, 3, 3, 2)),
        ];

        // Create the comparison and get the values.
        let compare = Compare {
            split: CompareSplit::Token,
            select: CompareSelect::Text,
            operations: Vec::new(),
        };
        let values: Vec<CompareValue> = compare.values(&tokens);

        assert_eq!(2, values.len());
        assert_eq!("t1", values[0].value);
        assert_eq!("t2", values[1].value);
    }

    #[test]
    fn split_token_select_text_with_replacement()
    {
        // Create the list of tokens, assuming it is already scoped properly.
        let input = Arc::new(String::from("t1 t2"));
        let tokens = vec![
            Token::new(input.clone(), Span::with_ascii(0, 0, 0, 2)),
            Token::new(input.clone(), Span::with_ascii(3, 3, 3, 2)),
        ];

        // Create the comparison and get the values.
        let compare = Compare {
            split: CompareSplit::Token,
            select: CompareSelect::Text,
            operations: vec![CompareOperation::ReplaceRegex(
                ReplaceRegex::with_search_replace(r"^(\w+?)\d+$", "$1"),
            )],
        };
        let values: Vec<CompareValue> = compare.values(&tokens);

        assert_eq!(2, values.len());
        assert_eq!("t", values[0].value);
        assert_eq!("t", values[1].value);
    }
}
