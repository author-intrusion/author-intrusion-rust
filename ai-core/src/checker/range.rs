use optional_struct::*;
use serde::{Deserialize, Serialize};
use std::cmp;
use std::iter::Iterator;

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct Range
{
    pub before: Option<usize>,
    pub after: Option<usize>,
    pub around: usize,
}

impl Range
{
    pub fn new(around: usize) -> Range
    {
        Range {
            before: None,
            after: None,
            around,
        }
    }

    pub fn with_before_after(before: usize, after: usize) -> Range
    {
        Range {
            before: Some(before),
            after: Some(after),
            around: 0,
        }
    }

    pub fn to_iter(&self, length: usize) -> RangeIter
    {
        let before = match self.before
        {
            None => self.around,
            Some(b) => b,
        };
        let after = match self.after
        {
            None => self.around,
            Some(a) => a,
        };

        RangeIter {
            length: length,
            before: before,
            after: after,
            current: 0,
        }
    }
}

#[derive(Debug)]
pub struct RangeIter
{
    /// The current index inside the array.
    current: usize,

    // The number of items before the current to show.
    before: usize,

    // The numbrer of items after the current item to include.
    after: usize,

    // The total length of the source array.
    length: usize,
}

impl Iterator for RangeIter
{
    type Item = (usize, usize, usize);

    fn next(&mut self) -> Option<(usize, usize, usize)>
    {
        // If we are at the end of the list, we're done.
        if self.current >= self.length
        {
            // We are at the end of the list.
            return None;
        }

        // Figure out what the actual after point.
        let current_before = if self.current > self.before
        {
            self.current - self.before
        }
        else
        {
            0
        };

        // We might have an overflow, so just treat it as a max.
        let current_after = match self.current.checked_add(self.after)
        {
            None => self.length - 1,
            Some(ca) => cmp::min(ca, self.length - 1),
        };

        // If we are at the beginning, we just list the list.
        let results = Some((self.current, current_before, current_after));

        self.current += 1;

        results
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn single_item_with_no_before_and_no_after()
    {
        let range = Range::with_before_after(0, 0);
        let mut iter = range.to_iter(1);

        assert_eq!(Some((0, 0, 0)), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn single_item_with_short_before_and_no_after()
    {
        let range = Range::with_before_after(1, 0);
        let mut iter = range.to_iter(1);

        assert_eq!(Some((0, 0, 0)), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn single_item_with_no_before_and_short_after()
    {
        let range = Range::with_before_after(0, 1);
        let mut iter = range.to_iter(1);

        assert_eq!(Some((0, 0, 0)), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn short_items_with_no_before_and_no_after()
    {
        let range = Range::with_before_after(0, 0);
        let mut iter = range.to_iter(3);

        assert_eq!(Some((0, 0, 0)), iter.next());
        assert_eq!(Some((1, 1, 1)), iter.next());
        assert_eq!(Some((2, 2, 2)), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn short_items_with_short_before_and_no_after()
    {
        let range = Range::with_before_after(1, 0);
        let mut iter = range.to_iter(3);

        assert_eq!(Some((0, 0, 0)), iter.next());
        assert_eq!(Some((1, 0, 1)), iter.next());
        assert_eq!(Some((2, 1, 2)), iter.next());
        assert_eq!(None, iter.next());
    }

    #[test]
    fn short_items_with_no_before_and_short_after()
    {
        let range = Range::with_before_after(0, 1);
        let mut iter = range.to_iter(3);

        assert_eq!(Some((0, 0, 1)), iter.next());
        assert_eq!(Some((1, 1, 2)), iter.next());
        assert_eq!(Some((2, 2, 2)), iter.next());
        assert_eq!(None, iter.next());
    }
}
