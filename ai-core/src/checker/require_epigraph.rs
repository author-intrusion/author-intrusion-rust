use super::common::*;
use crate::checker::issue::Issue;
use crate::checker::level::Level;
use crate::project::file::content::values::ProjectFileContentValues;
use crate::project::location::Location;
use crate::token::span::Span;
use optional_struct::*;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

/// A checker that looks for a block quote starting the content.
#[derive(Debug, Clone, OptionalStruct)]
#[opt_nested_optional = true]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
#[opt_nested_original(Common)]
#[opt_nested_generated(OptionalCommon)]
pub struct RequireEpigraph
{
    common: Common,
}

impl RequireEpigraph
{
    pub fn new() -> RequireEpigraph
    {
        RequireEpigraph {
            common: Common::new("epigraph"),
        }
    }

    pub fn with_options(options: OptionalRequireEpigraph) -> RequireEpigraph
    {
        let mut checker = RequireEpigraph::new();
        checker.apply_options(options);
        checker
    }

    pub fn check(
        &self,
        loc: Arc<Location>,
        content: &ProjectFileContentValues,
    ) -> Vec<Issue>
    {
        // If it starts with a ">", we don't have an issue.
        let found = &content.text.trim().starts_with("> ");

        if *found
        {
            return Vec::new();
        }

        // Otherwise, we need to create a new issue.
        let key = &self.common.key;
        let span = Span::with_ascii(content.line_index, 0, 0, 1);
        let issue = Issue::new(
            loc,
            span,
            Level::Error,
            key.clone(),
            String::from("required epigraph is missing"),
        );

        vec![issue]
    }
}

#[cfg(test)]
mod tests
{
    use crate::checker::level::*;
    use crate::checker::require_epigraph::RequireEpigraph;
    use crate::project::file::content::values::ProjectFileContentValues;
    use crate::project::location::Location;
    use std::sync::Arc;

    #[test]
    fn blank_input()
    {
        let loc = Arc::new(Location::with_blank());
        let content = ProjectFileContentValues::from_str("");
        let checker = RequireEpigraph::new();
        let results = checker.check(loc, &content);

        assert_eq!(1, results.len());
        assert_eq!(
            &("required epigraph is missing", Level::Error),
            &results[0].to_text_level()
        );
    }

    #[test]
    fn leading_newline()
    {
        let loc = Arc::new(Location::with_blank());
        let content = ProjectFileContentValues::from_str("\n> Epigraph");
        let checker = RequireEpigraph::new();
        let results = checker.check(loc, &content);

        assert_eq!(0, results.len());
    }

    #[test]
    fn epigraph()
    {
        let loc = Arc::new(Location::with_blank());
        let content = ProjectFileContentValues::from_str("> Epigraph");
        let checker = RequireEpigraph::new();
        let results = checker.check(loc, &content);

        assert_eq!(0, results.len());
    }
}
