use super::level::*;
use crate::project::location::Location;
use crate::token::span::Span;
use crate::token::Token;
use core::cmp::{Ord, Ordering};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct Issue
{
    pub loc: Arc<Location>,
    pub span: Span,
    pub level: Level,
    pub key: String,
    pub text: String,
}

impl Issue
{
    pub fn new(
        loc: Arc<Location>,
        span: Span,
        level: Level,
        key: String,
        text: String,
    ) -> Issue
    {
        let loc = loc.clone();

        Issue {
            loc,
            span,
            level,
            key,
            text,
        }
    }

    pub fn new_with_token(
        loc: Arc<Location>,
        token: &Token,
        level: Level,
        key: String,
        text: String,
    ) -> Issue
    {
        Issue {
            loc,
            span: token.span,
            level,
            key,
            text,
        }
    }

    #[cfg(test)]
    pub fn to_text_level(&self) -> (&str, Level)
    {
        let text = &self.text;
        let level = &self.level;
        let level = level.clone();

        (text, level)
    }
}

impl Eq for Issue {}

impl Ord for Issue
{
    fn cmp(&self, other: &Issue) -> Ordering
    {
        match self.loc.cmp(&other.loc)
        {
            Ordering::Less => Ordering::Less,
            Ordering::Greater => Ordering::Greater,
            _ => self.span.cmp(&other.span),
        }
    }
}

impl PartialOrd for Issue
{
    fn partial_cmp(&self, other: &Issue) -> Option<Ordering>
    {
        Some(self.cmp(&other))
    }
}

impl PartialEq for Issue
{
    fn eq(&self, other: &Issue) -> bool
    {
        self.span == other.span
    }
}
