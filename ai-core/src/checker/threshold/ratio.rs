use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Ratio
{
    pub warn: f32,
    pub err: f32,
}

impl Ratio
{
    pub fn new(warn: f32, err: f32) -> Ratio
    {
        Ratio { warn, err }
    }
}
