use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct Count
{
    pub warn: u32,
    pub err: u32,
}

impl Count
{
    pub fn new(warn: u32, err: u32) -> Count
    {
        Count { warn, err }
    }
}
