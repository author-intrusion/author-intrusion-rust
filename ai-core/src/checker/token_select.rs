/// Indicates the scope of processing in a file.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum TokenSelect
{
    Text,
}

impl TokenSelect
{
    pub fn new(_value: &Option<String>) -> TokenSelect
    {
        TokenSelect::Text
    }
}
