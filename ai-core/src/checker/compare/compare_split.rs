use crate::token::Token;
use serde::{Deserialize, Serialize};

/// Indicates the type of objects we're going to be comparing with each other.
#[derive(
    Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize,
)]
pub enum CompareSplit
{
    #[serde(alias = "token")]
    Token,
}

impl CompareSplit
{
    pub fn to_iter(&self, tokens: &Vec<Token>) -> CompareSplitIter
    {
        let tokens: Vec<Token> =
            tokens.into_iter().map(|t| t.clone()).collect();

        CompareSplitIter { tokens, index: 0 }
    }
}

pub struct CompareSplitIter
{
    tokens: Vec<Token>,
    index: usize,
}

impl Iterator for CompareSplitIter
{
    type Item = Vec<Token>;

    fn next(&mut self) -> Option<Vec<Token>>
    {
        // If we've gone past the index, then we're done.
        if &self.index >= &self.tokens.len()
        {
            return None;
        }

        // Grab the next one.
        let value = &self.tokens[self.index];
        let value = value.clone();
        let value = vec![value];

        self.index += 1;

        Some(value)
    }
}
