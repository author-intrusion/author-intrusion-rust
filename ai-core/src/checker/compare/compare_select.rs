use crate::token::Token;
use serde::{Deserialize, Serialize};

/// Indicates the processing for the value to convert it into a normalized form.
/// The two most common are case-sensitive values (Text) and case-insensitive
/// (Lowercase).
#[derive(
    Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize,
)]
pub enum CompareSelect
{
    #[serde(alias = "text")]
    Text,

    #[serde(alias = "lc")]
    #[serde(alias = "lower")]
    #[serde(alias = "lowercase")]
    Lowercase,
}

impl CompareSelect
{
    pub fn value(&self, tokens: &Vec<Token>) -> String
    {
        String::from(tokens[0].text())
    }
}
