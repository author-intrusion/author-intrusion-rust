use crate::checker::compare::compare_value::*;
use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct FilterList
{
    filter: Vec<String>,
}

impl FilterList
{
    pub fn new() -> FilterList
    {
        FilterList { filter: Vec::new() }
    }

    pub fn with_options(options: OptionalFilterList) -> FilterList
    {
        let mut results = FilterList::new();
        results.apply_options(options);
        results
    }

    pub fn filter(&self, value: &mut CompareValue) -> bool
    {
        self.filter.contains(&value.value)
    }
}

impl OptionalFilterList
{
    pub fn new(filter: Vec<&str>) -> OptionalFilterList
    {
        OptionalFilterList {
            filter: Some(filter.into_iter().map(|f| f.to_owned()).collect()),
        }
    }
}

// TODO: Write some tests.
