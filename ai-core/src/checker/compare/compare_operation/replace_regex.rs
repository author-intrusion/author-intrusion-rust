use super::regex_container::*;
use crate::checker::compare::compare_value::*;
use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct ReplaceRegex
{
    search: String,
    replace: String,
}

impl ReplaceRegex
{
    pub fn new() -> ReplaceRegex
    {
        ReplaceRegex::with_search_replace("^$", "")
    }

    pub fn with_search_replace(search: &str, replace: &str) -> ReplaceRegex
    {
        ReplaceRegex {
            search: search.to_owned(),
            replace: replace.to_owned(),
        }
    }

    pub fn with_options(options: OptionalReplaceRegex) -> ReplaceRegex
    {
        let mut results = ReplaceRegex::new();
        results.apply_options(options);
        results
    }

    pub fn update(&self, value: &mut CompareValue)
    {
        value.value = self.replace(&value.value);
    }

    pub fn replace(&self, input: &String) -> String
    {
        // We use a cache to avoid compiling the regular expressions ahead of
        // time when we cannot know them before the configuration file is
        // loaded.
        let search = &self.search;
        let regex = RegexCacheContainer::get(search);

        regex
            .replace(input, self.replace.as_str())
            .to_owned()
            .to_string()
    }
}

impl OptionalReplaceRegex
{
    pub fn new(search: &str, replace: &str) -> OptionalReplaceRegex
    {
        OptionalReplaceRegex {
            search: Some(search.to_owned()),
            replace: Some(replace.to_owned()),
        }
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn replace_string()
    {
        let replacement = ReplaceRegex {
            search: r"^(\w+?)\d+$".to_owned(),
            replace: "$1".to_owned(),
        };
        let input = "a1".to_owned();
        let results = replacement.replace(&input);

        assert_eq!("a", results);
    }
}
