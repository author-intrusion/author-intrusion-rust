use crate::checker::compare::compare_value::*;
use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct Lowercase {}

impl Lowercase
{
    pub fn new() -> Lowercase
    {
        Lowercase {}
    }

    pub fn with_options(options: OptionalLowercase) -> Lowercase
    {
        let mut results = Lowercase::new();
        results.apply_options(options);
        results
    }

    pub fn update(&self, value: &mut CompareValue)
    {
        value.value = value.value.to_lowercase();
    }
}

impl OptionalLowercase
{
    pub fn new() -> OptionalLowercase
    {
        OptionalLowercase {}
    }
}

// TODO: Write some tests.
