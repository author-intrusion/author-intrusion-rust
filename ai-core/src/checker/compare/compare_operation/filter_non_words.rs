use crate::checker::compare::compare_value::*;
use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct FilterNonWords {}

impl FilterNonWords
{
    pub fn new() -> FilterNonWords
    {
        FilterNonWords {}
    }

    pub fn with_options(options: OptionalFilterNonWords) -> FilterNonWords
    {
        let mut results = FilterNonWords::new();
        results.apply_options(options);
        results
    }

    pub fn filter(&self, value: &mut CompareValue) -> bool
    {
        !value.first().is_word()
    }
}

impl OptionalFilterNonWords
{
    pub fn new() -> OptionalFilterNonWords
    {
        OptionalFilterNonWords {}
    }
}

// TODO: Write some tests.
