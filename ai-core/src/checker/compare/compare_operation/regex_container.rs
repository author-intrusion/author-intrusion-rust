use lazy_static::*;
use regex::Regex;
use regex_cache::RegexCache;
use std::sync::*;

pub struct RegexCacheContainer {}

impl RegexCacheContainer
{
    pub fn get(input: &String) -> Regex
    {
        lazy_static! {
            static ref CACHE: Arc<RwLock<RegexCache>> =
                Arc::new(RwLock::new(RegexCache::new(100)));
        }

        CACHE.write().unwrap().compile(input).unwrap().to_owned()
    }
}
