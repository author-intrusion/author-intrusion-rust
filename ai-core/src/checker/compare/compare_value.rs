use crate::token::Token;

/// Describes a specific instance of a comparison that includes the tokens
/// split out by the `split` property of the parent class and modified by the
/// `select` property.
#[derive(Debug)]
pub struct CompareValue
{
    /// Contains the manipulated value from the given select.
    pub value: String,

    /// Contains the tokens that represent the split out object.
    pub tokens: Vec<Token>,

    /// Controls a flag whether this value should be ignored for processing
    /// but included for comparisons.
    pub skip_processing: bool,
}

impl CompareValue
{
    pub fn first(&self) -> &Token
    {
        &self.tokens[0]
    }
}
