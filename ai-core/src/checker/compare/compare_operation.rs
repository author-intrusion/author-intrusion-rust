use serde::{Deserialize, Serialize};

pub mod filter_list;
pub mod filter_non_words;
pub mod lowercase;
pub mod regex_container;
pub mod replace_regex;

#[derive(Debug, Clone)]
pub enum CompareOperation
{
    FilterList(filter_list::FilterList),
    FilterNonWords(filter_non_words::FilterNonWords),
    Lowercase(lowercase::Lowercase),
    ReplaceRegex(replace_regex::ReplaceRegex),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum OptionalCompareOperation
{
    #[serde(rename = "filter_list")]
    FilterList(filter_list::OptionalFilterList),

    #[serde(rename = "filter_non_words")]
    FilterNonWords(filter_non_words::OptionalFilterNonWords),

    #[serde(rename = "lowercase")]
    Lowercase(lowercase::OptionalLowercase),

    #[serde(rename = "replace_regex")]
    ReplaceRegex(replace_regex::OptionalReplaceRegex),
}
