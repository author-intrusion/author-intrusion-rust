use optional_struct::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
pub struct Common
{
    pub key: String,
}

impl Common
{
    pub fn new(key: &str) -> Common
    {
        Common {
            key: String::from(key),
        }
    }
}
