extern crate test;

use super::common::*;
use crate::checker::compare::compare_value::*;
use crate::checker::compare::*;
use crate::checker::issue::Issue;
use crate::checker::level::Level;
use crate::checker::range::*;
use crate::checker::scope::*;
use crate::checker::threshold::count::*;
use crate::project::location::Location;
use crate::token::Token;
use optional_struct::*;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

/// A checker looking for one or more word patterns that are overused.
#[derive(Debug, Clone, OptionalStruct)]
#[optional_derive(Debug, Serialize, Deserialize, Clone)]
#[opt_nested_optional = true]
#[opt_nested_original(Common)]
#[opt_nested_generated(OptionalCommon)]
#[opt_nested_original(Count)]
#[opt_nested_generated(OptionalCount)]
#[opt_nested_original(Compare)]
#[opt_nested_generated(OptionalCompare)]
#[opt_nested_original(Range)]
#[opt_nested_generated(OptionalRange)]
pub struct Clustered
{
    common: Common,
    threshold: Count,
    scope: Scope,
    compare: Compare,
    range: Range,
}

impl Clustered
{
    pub fn new() -> Clustered
    {
        Clustered {
            common: Common::new("clustered"),
            threshold: Count::new(2, 5),
            scope: Scope::File,
            compare: Compare::new(),
            range: Range::new(15),
        }
    }

    pub fn with_options(options: OptionalClustered) -> Clustered
    {
        let mut checker = Clustered::new();
        checker.apply_options(options);
        checker
    }

    pub fn check(&self, loc: Arc<Location>, tokens: &[Token]) -> Vec<Issue>
    {
        // We need to wrap the tokens into a wrapper that needs additional data
        // for processing.
        let tokens = &tokens.to_vec();

        if &tokens.len() == &0
        {
            return Vec::new();
        }

        // Go through and process each scope of the file.
        let issues: Vec<Issue> = self
            .scope
            .to_iter(tokens)
            .map(|scoped_tokens| self.compare.values(&scoped_tokens))
            .map(|compare_values| {
                let issues: Vec<Issue> = self
                    .range
                    .to_iter(compare_values.len())
                    .map(|(current, before, after)| {
                        let current_value = &compare_values[current];
                        let subset_values = &compare_values[before..=after];
                        let matched_values: Vec<&CompareValue> = subset_values
                            .iter()
                            .filter(|v| v.value == current_value.value)
                            .collect();
                        let count = matched_values.len();
                        let warn_threshold = self.threshold.warn as usize;
                        let err_threshold = self.threshold.err as usize;

                        // Subtract one because we don't want to include
                        // ourselves.
                        let count = count - 1;

                        // If we don't have anything to report, then we're done.
                        if count == 0 || count < warn_threshold
                        {
                            return None;
                        }

                        // This is going to be an issue, so create an issue.
                        let current_token = &current_value.tokens[0];
                        let level = match count < err_threshold
                        {
                            true => Level::Warning,
                            false => Level::Error,
                        };
                        let self_before = match self.range.before
                        {
                            None => self.range.around,
                            Some(a) => a,
                        };
                        let self_after = match self.range.after
                        {
                            None => self.range.around,
                            Some(a) => a,
                        };
                        let message =
                            format!(
                            "found {} excessive clustered {} matching {:?}{}",
                            count,
                            if count == 1 { "token" } else { "tokens" },
                            &current_token.text(),
                            if self_before == self_after
                            {
                                if self_before == std::usize::MAX
                                {
                                    String::from("")
                                }
                                else
                                {
                                    format!(" within {} token{}", self_before,
                                    if self_before == 1 { "" } else { "s" })
                                }
                            }
                            else
                            {
                                format!(
                                    " within tokens {} before and {} after",
                                    self_before, self_after
                                )
                            },
                        );

                        Some(Issue::new_with_token(
                            loc.clone(),
                            &current_token,
                            level.clone(),
                            self.common.key.clone(),
                            message,
                        ))
                    })
                    .filter_map(|x| x)
                    .collect();

                issues
            })
            .flatten()
            .collect();

        issues
    }
}

#[cfg(test)]
mod tests
{
    use super::*;
    use crate::checker::compare::compare_operation::replace_regex::*;
    use crate::checker::compare::compare_operation::*;
    use crate::checker::level::*;
    use crate::project::location::Location;
    use crate::token::builder::Builder;
    use std::sync::Arc;
    use test::Bencher;

    #[test]
    fn parse_empty_simple()
    {
        let results = a_prefix_results("");

        assert_eq!(0, results.len());
    }

    #[test]
    fn parse_simple_negative()
    {
        let results = a_prefix_results("a1 b2 c3 d4 e5");

        assert_eq!(0, results.len());
    }

    #[test]
    fn parse_simple_warn()
    {
        let results = a_prefix_results("a1 a2 c3 d4 e5");

        assert_eq!(2, results.len());
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a1\" within 15 tokens",
                Level::Warning
            ),
            &results[0].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a2\" within 15 tokens",
                Level::Warning
            ),
            &results[1].to_text_level()
        );
    }

    #[test]
    fn parse_simple_err()
    {
        let results = a_prefix_results_threshold("a1 a2 c3 a4 a5", 1, 2);

        assert_eq!(4, results.len());
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a1\" within 15 tokens",
                Level::Error
            ),
            &results[0].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a2\" within 15 tokens",
                Level::Error
            ),
            &results[1].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a4\" within 15 tokens",
                Level::Error
            ),
            &results[2].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a5\" within 15 tokens",
                Level::Error
            ),
            &results[3].to_text_level()
        );
    }

    #[test]
    fn consolidated_search()
    {
        let results = a_prefix_results("a1 a2 c3 a4 a5 c6");

        assert_eq!(6, results.len());
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a1\" within 15 tokens",
                Level::Error
            ),
            &results[0].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a2\" within 15 tokens",
                Level::Error
            ),
            &results[1].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"c3\" within 15 tokens",
                Level::Warning
            ),
            &results[2].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a4\" within 15 tokens",
                Level::Error
            ),
            &results[3].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a5\" within 15 tokens",
                Level::Error
            ),
            &results[4].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"c6\" within 15 tokens",
                Level::Warning
            ),
            &results[5].to_text_level()
        );
    }

    #[test]
    fn consolidated_search_short()
    {
        let results = a_prefix_results_range("a1 a2 c3 a4 a5 c6", 1, 1);

        assert_eq!(4, results.len());
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a1\" within 1 token",
                Level::Warning
            ),
            &results[0].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a2\" within 1 token",
                Level::Warning
            ),
            &results[1].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a4\" within 1 token",
                Level::Warning
            ),
            &results[2].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"a5\" within 1 token",
                Level::Warning
            ),
            &results[3].to_text_level()
        );
    }

    #[test]
    fn consolidated_search_without_limit()
    {
        let results = a_prefix_results_range(
            "a1 a2 c3 a4 a5 c6",
            std::usize::MAX,
            std::usize::MAX,
        );

        assert_eq!(6, results.len());
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a1\"",
                Level::Error
            ),
            &results[0].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a2\"",
                Level::Error
            ),
            &results[1].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"c3\"",
                Level::Warning
            ),
            &results[2].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a4\"",
                Level::Error
            ),
            &results[3].to_text_level()
        );
        assert_eq!(
            &(
                "found 3 excessive clustered tokens matching \"a5\"",
                Level::Error
            ),
            &results[4].to_text_level()
        );
        assert_eq!(
            &(
                "found 1 excessive clustered token matching \"c6\"",
                Level::Warning
            ),
            &results[5].to_text_level()
        );
    }

    #[bench]
    fn large_token_count_1000(b: &mut Bencher)
    {
        // Create a string with lots of tokens.
        let mut input = String::with_capacity(1000 * 10);

        input += "0";

        for x in 0..1000
        {
            input += " ";
            input += &x.to_string();
        }

        let input = &input;

        // Perform the testing on the actual parsing.
        b.iter(move || a_prefix_results(input));
    }

    #[bench]
    fn large_token_count_10000(b: &mut Bencher)
    {
        // Create a string with lots of tokens.
        let mut input = String::with_capacity(1000 * 10);

        input += "0";

        for x in 0..10000
        {
            input += " ";
            input += &x.to_string();
        }

        let input = &input;

        // Perform the testing on the actual parsing.
        b.iter(move || a_prefix_results(input));
    }

    #[bench]
    fn large_token_count_full_1000(b: &mut Bencher)
    {
        // Create a string with lots of tokens.
        let mut input = String::with_capacity(1000 * 10);

        input += "0";

        for x in 0..1000
        {
            input += " ";
            input += &x.to_string();
        }

        let input = &input;

        // Perform the testing on the actual parsing.
        b.iter(|| {
            a_prefix_results_range(input, std::usize::MAX, std::usize::MAX);
        });
    }

    fn new_a_prefix_data(
        warn: u32,
        err: u32,
        before: usize,
        after: usize,
    ) -> Clustered
    {
        Clustered::with_options(OptionalClustered {
            common: None,
            threshold: Some(OptionalCount {
                warn: Some(warn),
                err: Some(err),
            }),
            scope: None,
            compare: Some(OptionalCompare {
                split: None,
                select: None,
                operations: Some(vec![OptionalCompareOperation::ReplaceRegex(
                    OptionalReplaceRegex::new(r"^(\w+)\d+$", "$1"),
                )]),
            }),
            range: Some(OptionalRange {
                before: Some(before),
                after: Some(after),
                around: None,
            }),
        })
    }

    fn a_prefix_results(input: &str) -> Vec<Issue>
    {
        a_prefix_results_full(input, 1, 3, 15, 15)
    }

    fn a_prefix_results_range(
        input: &str,
        before: usize,
        after: usize,
    ) -> Vec<Issue>
    {
        a_prefix_results_full(input, 1, 3, before, after)
    }

    fn a_prefix_results_threshold(
        input: &str,
        warn: u32,
        err: u32,
    ) -> Vec<Issue>
    {
        a_prefix_results_full(input, warn, err, 15, 15)
    }

    fn a_prefix_results_full(
        input: &str,
        warn: u32,
        err: u32,
        before: usize,
        after: usize,
    ) -> Vec<Issue>
    {
        test_logger::ensure_env_logger_initialized();

        let input = Arc::new(String::from(input));
        let loc = Arc::new(Location::with_blank());
        let tokens = Builder::build(input, 0);
        let checker = new_a_prefix_data(warn, err, before, after);
        let results = checker.check(loc, &tokens);

        results
    }
}
