use core::cmp::Ordering;

#[derive(Debug, Clone, Eq)]
pub enum Level
{
    Warning,
    Error,
}

impl Level
{
    pub fn as_str(&self) -> &str
    {
        match self
        {
            Level::Warning => "warning",
            Level::Error => "error",
        }
    }
}

impl Ord for Level
{
    fn cmp(&self, other: &Level) -> Ordering
    {
        self.as_str().cmp(&other.as_str())
    }
}

impl PartialOrd for Level
{
    fn partial_cmp(&self, other: &Level) -> Option<Ordering>
    {
        Some(self.as_str().cmp(other.as_str()))
    }
}

impl PartialEq for Level
{
    fn eq(&self, other: &Level) -> bool
    {
        self.as_str() == other.as_str()
    }
}
