use serde::{Deserialize, Serialize};

pub mod clustered;
pub mod common;
pub mod compare;
pub mod issue;
pub mod level;
pub mod range;
pub mod require_epigraph;
pub mod scope;
pub mod threshold;
pub mod token_select;

#[derive(Debug, Clone)]
pub enum Checker
{
    Clustered(clustered::Clustered),
    RequireEpigraph(require_epigraph::RequireEpigraph),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum OptionalChecker
{
    #[serde(rename = "clustered")]
    Clustered(clustered::OptionalClustered),

    #[serde(rename = "require_epigraph")]
    RequireEpigraph(require_epigraph::OptionalRequireEpigraph),
}
