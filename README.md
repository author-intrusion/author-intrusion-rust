# Author Intrusion

Author Intrusion is a utility for authors who want to treat their writing like a programmer works with source code. It is modeled after various other tools such as `git`, `npm`, and `dotnet` in that it provides many functions for querying, manipulating, and refactoring the source, in this case Markdown files with YAML front matter.

In specific, the goals of the project include:

* *Linting* text to help identify overused words or phrases, echoed words, finding incorrect tenses, or project-specific settings (such as requiring an epigraph in the front of every chapter).
* *Metadata* to allow queries about a chapter without requiring a separate spreadsheet or note file.
* *Refactoring* to allow renaming characters across all chapters and even in notes.
* *Find Usages* to find where a character or location is referenced to make it easier to review or edit.
* *Language Services* to allow it to be hooked into other editors, such as Atom or Emacs, to treat writing as a "language" with highlighting for names or going to character notes.

In short, this is a tool for treating writing like source code and providing methods to work with them that would otherwise be provided by tools like Microsoft Word or Scrivner.

## Cookbook

Most of these examples assume that `author-intrusion` is on your `PATH`. When
running while developing, we recommend the following alias because it works
even in the example folders (the CLI will search up the tree for a project
file):

```
$ alias author-intrusion="cargo run --quiet --"
```

### Chapter Listing

To list the chapter list with chapter numbers and word counts:

```
$ author-intrusion chapter list -f file.num -f title -f count.words
```

## Tasks

### General

- (TEST) Passing invalid directory into `--cwd`
- LSP
  - Add the file if not already in the system

### Medium Priority

- Create paragraphs from the lines
- Create scenes from the paragraphs
- Add `content count` command
  - Only need number columns
- Filter chapters from command line

### Low Priority

- What about scene-specific breakdown?
  - Scenes shouldn't have titles, how define a title?
  - `chapters/chapter-00/scene-00.md`
  - Probably low priority
  - Specifier: `00-00` or `00:00` or `00/00`
- World-specific timestamps
- Git integration
- Add formatting rules for the table formats
  - Decimals
- Add the ability to reduce it to `k`, `M`, `G` or `B`, etc?
- Add the ability to define titles for table
- Add integration tests for cell formatting for lists
- Writing
  - Write out YAML header
  - Handle word wrapping and formatting
  - Maximum line length
    - Defined in project file
    - Default is unlimited?

## Commands

- [ ] `chapter insert [--count 1] after 00`
- [ ] `stats`: Breakdown of various information about the entire project
      including information about individual chapters.
  - [ ] `--on-or-after 00`: Show information only for a given chapter
  - [ ] `--ge 00` (same)
  - [ ] `--le 00` (less than or equal to)
  - [ ] Needs to handle files checked into Git
- [ ] `check`: Perform analysis on the entire project.

## Selection

```
scope:       file # paragraph, sentence
threshold:
  warn:      2
  err:       5
range:
  before:    10
  after:     10
  around:    10
compare:
  split:     token # sentence, paragraph
  #filter:   token # word, nonword
  #range:    1-5
  select:    itext # text, stem, pos
  #replacements:
  #  - search: ^(\w+?)\d+$
  #    replace: $1

  # New Version
scope:       file # paragraph, sentence
threshold:   { warn: 2, err: 5 }
range:       { around: 10 }
compare:
  split:       token
  select:      text
  operations:
    - op:      exclude_data
    - op:      exclude_regex
      search:  ^(a|an|the)$
    - op:      lowercase
    - op:      replace_regex
      search:  ^(\w+?)\d+$
      replace: $1
```

* select.get_tokens

## overused_word

```
// Parse the pattern.
let pattern = &data.pattern.as_ref().unwrap().as_str();
let pattern = Regex::new(pattern).unwrap();
```

RangedScope
```
scope: file
range:
  around: 30
  before: 30
  after: 30
select: text
regex:
  search: ^(nod)(?:ding)$
  replace: $1
insensitive: true
```

## Development

Even though it isn't a Rust utility, we use NPM to use Husky and other checks
for checking in code and verification. This is to ensure we use [conventional
commits](https://www.conventionalcommits.org/) properly.

Beyond that, we use [cargo-make](https://sagiegurari.github.io/cargo-make/) for
our build system. This handles linting (via clippy), formatting, and testing.

```
$ npm install
$ cargo make
```

## Documentation

```
cargo doc --document-private-items
```
