use crate::ops::publish_diagnostics;
use ai_core::project::Project;
use crossbeam_channel::Sender;
use failure::Error;
use gen_lsp_server::{RawMessage, RawNotification};
use lsp_types::DidChangeWatchedFilesParams;
use std::path::PathBuf;
use std::result::Result;

pub fn notification(
    sender: &Sender<RawMessage>,
    project: &Project,
    params: Result<DidChangeWatchedFilesParams, RawNotification>,
) -> Result<(), Error>
{
    // Ignore errors, otherwise, pull out the proper parameters.
    if params.is_err()
    {
        return Ok(());
    }

    let params = params.unwrap();

    // Pull out the changes and get the smallest list of things to change.
    let mut paths: Vec<String> = params
        .changes
        .into_iter()
        .map(|x| String::from(x.uri.path()))
        .collect();

    paths.sort();
    paths.dedup();

    // Go through the changes and look for files that match.
    let files = &project.content();

    for path in paths
    {
        let path = PathBuf::from(path);
        let file = files.iter().find(|f| f.location.abs_path() == path);

        if let Some(file) = file
        {
            // We have a file, so we need to rebuild it and resend
            // notifications to the client.
            &file.reset();
            publish_diagnostics::publish_file(&file, &sender, true)?;
        }
    }

    // We're good, so pass it along.
    Ok(())
}
