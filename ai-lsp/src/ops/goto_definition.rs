use crossbeam_channel::Sender;
use failure::Error;
use gen_lsp_server::{RawMessage, RawRequest, RawResponse};
use log::*;
use lsp_types::{
    request::{GotoDefinition, GotoDefinitionResponse},
    TextDocumentPositionParams,
};
use std::result::Result;

pub fn request(
    sender: &Sender<RawMessage>,
    req: Result<(u64, TextDocumentPositionParams), RawRequest>,
) -> Result<(), Error>
{
    if let Ok((id, params)) = req
    {
        info!("got gotoDefinition request #{}: {:?}", id, params);
        let resp = RawResponse::ok::<GotoDefinition>(
            id,
            &Some(GotoDefinitionResponse::Array(Vec::new())),
        );
        info!("sending gotoDefinition response: {:?}", resp);
        sender.send(RawMessage::Response(resp))?;
    }

    Ok(())
}
