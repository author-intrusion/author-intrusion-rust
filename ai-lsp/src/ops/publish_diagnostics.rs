use ai_core::checker::level::Level;
use ai_core::project::file::File;
use ai_core::project::Project;
use crossbeam_channel::Sender;
use failure::Error;
use gen_lsp_server::{RawMessage, RawNotification};
use lsp_types::NumberOrString;
use lsp_types::{
    notification::PublishDiagnostics, Diagnostic, DiagnosticSeverity, Position,
    PublishDiagnosticsParams, Range,
};
use std::result::Result;
use url::Url;

pub fn publish_all(
    project: &Project,
    sender: &Sender<RawMessage>,
) -> Result<(), Error>
{
    let files = &project.content();

    for file in files
    {
        publish_file(file, sender, false)?;
    }

    Ok(())
}

pub fn publish_file(
    file: &File,
    sender: &Sender<RawMessage>,
    force_send: bool,
) -> Result<(), Error>
{
    // Get the individual errors and warnings on the page.
    let issues = &file.get_issues()?;
    let diags: Vec<Diagnostic> = issues
        .iter()
        .map(|issue| {
            let loc = &issue.span;
            let line = loc.line as u64;
            let offset = loc.c.line as u64;
            let len = loc.c.len as u64;
            let start = Position::new(line, offset);
            let end = Position::new(line, offset + len);
            let range = Range::new(start, end);

            let severity = match &issue.level
            {
                Level::Error => DiagnosticSeverity::Error,
                Level::Warning => DiagnosticSeverity::Warning,
            };
            let severity = Some(severity);

            let message = &issue.text;
            let message = message.clone();
            let key = issue.key.clone();
            let key = Some(NumberOrString::String(key));

            Diagnostic::new(range, severity, key, None, message, None)
        })
        .collect();

    // If we don't have any, then there is nothing to do.
    if !force_send && diags.len() == 0
    {
        return Ok(());
    }

    // Wrap them in the parameters and send them.
    let url = &file.location.abs_path();
    let url = url.clone();
    let url = url.into_os_string().into_string().unwrap();
    let url = format!("file://{}", url);
    let url = Url::parse(&url).unwrap();
    let params = PublishDiagnosticsParams::new(url, diags);
    let notice = RawNotification::new::<PublishDiagnostics>(&params);

    sender.send(RawMessage::Notification(notice))?;

    Ok(())
}
