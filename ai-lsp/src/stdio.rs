use crate::server::{get_capabilities, main_loop};
use ai_cmd::{result::CommandResult, CommandContext};
use failure;
use gen_lsp_server::{run_server, stdio_transport};
use log::*;

#[derive(Debug)]
pub struct StdioLanguageServerCommand
{
    pub context: CommandContext,
}

impl StdioLanguageServerCommand
{
    pub fn run(self) -> Result<CommandResult, failure::Error>
    {
        // Create the transport using stdio (stdin and stdout) for the IO. This
        // means we have to do all logging to stderr (which we've already set
        // up).
        let (receiver, sender, io_threads) = stdio_transport();

        // Run the server and wait for the two threads to end (typically by trigger LSP Exit event).
        let capabilities = get_capabilities();
        info!("starting stdio server");
        run_server(capabilities, receiver, sender, main_loop)?;
        io_threads.join()?;

        // Shut down gracefully and finish the command.
        info!("shutting down server");
        Ok(CommandResult::success())
    }
}
