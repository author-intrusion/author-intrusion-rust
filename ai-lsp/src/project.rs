use ai_core::project::fs;
use ai_core::project::Project;
use log::*;
use lsp_types::InitializeParams;
use std::path::PathBuf;

pub fn get_project(
    params: InitializeParams,
) -> Result<Option<Project>, failure::Error>
{
    match params.root_path
    {
        None =>
        {
            warn!("root_path was not provided, shutting down");
            Ok(None)
        }
        Some(project_path) =>
        {
            info!("using project path {:?}", project_path);

            let project_path = PathBuf::from(project_path);
            let project = fs::get_project(&project_path, None)?;

            &project.scan_files()?;

            Ok(Some(project))
        }
    }
}
