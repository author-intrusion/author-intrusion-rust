use crate::{
    ops::{did_change_watched_files, goto_definition, publish_diagnostics},
    project::get_project,
};
use crossbeam_channel::{Receiver, Sender};
use gen_lsp_server::{handle_shutdown, RawMessage};
use log::*;
use lsp_types::{
    notification::DidChangeWatchedFiles, request::GotoDefinition,
    InitializeParams, ServerCapabilities,
};

pub fn get_capabilities() -> ServerCapabilities
{
    let mut capabilities = ServerCapabilities::default();
    capabilities.hover_provider = Some(false);
    capabilities
}

pub fn main_loop(
    params: InitializeParams,
    receiver: &Receiver<RawMessage>,
    sender: &Sender<RawMessage>,
) -> Result<(), failure::Error>
{
    // As part of the initialization, we get a path to the workspace folder.
    // This is where we expect to see the project file but the nature of the LSP
    // client means we might be called for a project folder that isn't set up
    // for Author Intrusion. This can change, so we use `Option<Project>` to
    // track that and also handle file deletions, creations, and changes.
    let maybe_project = get_project(params)?;
    debug!("project {:?}", maybe_project);

    // If we have a project, we want to start with the initial diagnostics scan
    // which will show up errors on the page. Obviously, if we don't have a
    // project, then we aren't going to do anything.
    if let Some(project) = &maybe_project
    {
        publish_diagnostics::publish_all(&project, &sender)?;
    }

    // Once we're set up, then we loop through the (nearly infinite) receiver
    // calls until we get a shutdown request.
    for msg in receiver
    {
        match msg
        {
            RawMessage::Request(req) =>
            {
                // See if we have a shutdown request.
                let req = match handle_shutdown(req, sender)
                {
                    None => return Ok(()),
                    Some(req) => req,
                };

                // For everything else, process it in a match.
                trace!("got request: {:?}", req);

                goto_definition::request(sender, req.cast::<GotoDefinition>())?;
            }
            RawMessage::Response(resp) =>
            {
                info!("got response: {:?}", resp);
            }
            RawMessage::Notification(not) =>
            {
                trace!("got notification: {:?}", not);

                if let Some(project) = &maybe_project
                {
                    did_change_watched_files::notification(
                        sender,
                        &project,
                        not.cast::<DidChangeWatchedFiles>(),
                    )?;
                }
            }
        }
    }

    Ok(())
}
